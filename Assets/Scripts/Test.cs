﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

using UnityEngine.EventSystems;
using System.IO;
using System.Text;

using System.Runtime.InteropServices;

public class Test : MonoBehaviour
{
    public LabelPickerWindow labelPicker;

    [SerializeField]
    private Texture2D[] seeds;

    [SerializeField]
    private Transform cursor;

    [SerializeField]
    private LineRenderer outline;

    [SerializeField]
    private Font font;

    [SerializeField]
    private SpriteRenderer playerRenderer;

    [SerializeField]
    private EditorHUD hud;

    public Texture2D testTileset;
    public Texture2D paletteTexture;

    public Context context;

    public static TextureByte.Sprite blankSprite;

    private List<BlockType> types;

    private Dictionary<string, BlockShape> shapes
        = new Dictionary<string, BlockShape>();
    private Dictionary<BlockShape, string> shapeNames
        = new Dictionary<BlockShape, string>();

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void UpdateSearchWithID(string id);

    [DllImport("__Internal")]
    private static extern string GetWindowSearch();
#else
    private static void UpdateSearchWithID(string id) { }
#endif

    private IEnumerator LoadBlock(List<BlockShape> blocks, string name)
    {
        ShapeData data;

        var request = new WWW($"{Files.DefaultShapesRoot}/{name}.json");

        yield return request;

        data = JSON.Deserialize<ShapeData>(request.text);

        //Files.Load($"{Files.DefaultShapesRoot}/{name}.json", out data);

        data.name = name;

        var shape = ShapeData.ToBlockShape(data);

        shapes.Add(name, shape);
        shapeNames.Add(shape, name);

        blocks.Add(shape);

        for (int i = 0; i < shape.vertices.Count; ++i)
        {
            shape.vertices[i] -= Vector3.one * 0.5f;
        }

        yield break;
    }

    private List<BlockShape> blockShapes = new List<BlockShape>();

    private IEnumerator RandomisePalette(Palette palette)
    {
        var prev = palette.colors.ToList();
        var next = Enumerable.Range(0, 256).Select(i => new Color(Random.value, Random.value, Random.value)).ToList();

        float duration = 1f;
        float t = 0f;

        do
        {
            t = Mathf.Min(duration, t + Time.deltaTime);
            float u = t / duration;

            for (int i = 1; i < 256; ++i)
            {
                palette.SetColor(i, Color.Lerp(prev[i], next[i], u), false);
            }

            palette.SetColor(0, Color.clear);
            palette.Apply();

            yield return null;
        }
        while (t < duration);
    }

    private int collisionMask;
    private int blocksMask;
    private int npcsMask;
    private IEnumerator Start()
    {
        font.material.mainTexture.filterMode = FilterMode.Point;

        npcsMask = LayerMask.GetMask("NPCs");
        blocksMask = LayerMask.GetMask("Cubes");
        collisionMask = LayerMask.GetMask("Floor", "Passable");

        context = new Context
        {
            palette = new Palette(paletteTexture),
            texture = new TextureByte(2048, 2048),
            sprites = new List<TextureByte.Sprite>(),
        };

        context.texture.SetPixels32(testTileset.GetPixels32());
        context.texture.Apply();

        for (int y = 0; y < 60; ++y)
        {
            for (int x = 0; x < 60; ++x)
            {
                var sprite = context.texture.GetSprite(new IntRect(x * 34 + 1, y * 34 + 1, 32, 32), IntVector2.one * 16);
                sprite.SetPixelsPerUnit(32);
                context.sprites.Add(sprite);
            }
        }

        blankSprite = context.sprites[0];

        yield return null;

        yield return StartCoroutine(LoadBlock(blockShapes, "cube"));
        yield return StartCoroutine(LoadBlock(blockShapes, "half"));
        yield return StartCoroutine(LoadBlock(blockShapes, "slope"));
        yield return StartCoroutine(LoadBlock(blockShapes, "corner"));
        yield return StartCoroutine(LoadBlock(blockShapes, "midramp1"));
        yield return StartCoroutine(LoadBlock(blockShapes, "midramp2"));
        yield return StartCoroutine(LoadBlock(blockShapes, "pyramid"));

        room.blocks = new List<Block>();

        int j = 0;

        types = Enumerable.Range(0, 2).SelectMany(i => blockShapes.Select(shape =>
        {
            var type = new BlockType(shape);

            foreach (var pair in type.shape.shapes)
            {
                j += 1;
                type.TextureShape(pair.Key, context.sprites[j], SquareOrientation.None0);
            }

            return type;
        })).ToList();


        LoadRoom("test");
        context.sprites[0].Clear();
        context.texture.Apply();

        StartCoroutine(FakeAnimation());

        playerRenderer.sprite = context.sprites[1].uSprite;
    }

    public RoomView world;

    public Vector2 ScreenCenter => Camera.main.pixelRect.size * 0.5f;
    private int blocki;

    private Block lastBlock;

    public Room room;

    [SerializeField]
    private MouseLook mouselook1, mouselook2;

    public bool Playing;

    [Header("Windows")]
    public TileEditorWindow tileEditor;
    public TilePickerWindow tilePicker;
    public BlockPickerWindow blockPicker;
    public CharacterEditWindow characterEditor;

    public void Rotate(Block block, Vector3 axis, int turns)
    {
        var quaternion = Quaternion.Euler(block.rotation1 * 90,
                                          block.rotation2 * 90,
                                          block.rotation3 * 90);
        quaternion = Quaternion.AngleAxis(turns * 90, axis) * quaternion;
        block.rotation1 = Mathf.RoundToInt(quaternion.eulerAngles.x / 90);
        block.rotation2 = Mathf.RoundToInt(quaternion.eulerAngles.y / 90);
        block.rotation3 = Mathf.RoundToInt(quaternion.eulerAngles.z / 90);
    }

    private IntVector3 cursorPosition;

    private Dictionary<KeyCode, IntVector3> axes
        = new Dictionary<KeyCode, IntVector3>
        {
            [KeyCode.W] = IntVector3.left,
            [KeyCode.S] = IntVector3.right,
            [KeyCode.Q] = IntVector3.forward,
            [KeyCode.E] = IntVector3.back,
            [KeyCode.D] = IntVector3.down,
            [KeyCode.A] = IntVector3.up,
        };

    private Dictionary<KeyCode, IntVector3> keymap
        = new Dictionary<KeyCode, IntVector3>
        {
            [KeyCode.A] = IntVector3.left,
            [KeyCode.D] = IntVector3.right,
            [KeyCode.W] = IntVector3.forward,
            [KeyCode.S] = IntVector3.back,
            [KeyCode.Q] = IntVector3.down,
            [KeyCode.E] = IntVector3.up,
        };

    public static List<KeyCode> numberKeys = new List<KeyCode>
    {
        KeyCode.Alpha0,
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9,
    };

    private static List<RaycastResult> uihits = new List<RaycastResult>();
    private static PointerEventData pointer;
    public static bool IsPointBlocked(Vector2 point)
    {
        pointer = pointer ?? new PointerEventData(EventSystem.current);
        pointer.position = point;

        uihits.Clear();
        EventSystem.current.RaycastAll(pointer, uihits);

        return uihits.Count > 0;
    }

    public void OpenBlockSelect()
    {
        blockPicker.Open(t => blocki = types.IndexOf(t), types.Take(8 * 8).ToList());
    }

    public void FrameFlipH(OrientedTileEntry entry)
    {
        entry.orientation = entry.orientation.FlippedH();
    }

    public void FrameFlipV(OrientedTileEntry entry)
    {
        entry.orientation = entry.orientation.FlippedV();
    }

    private void FrameEditorUpdate(OrientedTileEntry entry, Action refresh)
    {
        if (Input.GetKeyDown(KeyCode.Comma))
        {
            tileEditor.SetCanvas(entry.sprite, context.palette);
            tileEditor.gameObject.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Period))
        {
            tilePicker.Open(sprite =>
            {
                entry.sprite = sprite;
                refresh?.Invoke();
            }, context.sprites.Take(8 * 8).ToList());
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            entry.orientation = entry.orientation.RotatedCW1();
            refresh?.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.H))
        {
            FrameFlipH(entry);
            refresh?.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.V))
        {
            FrameFlipV(entry);
            refresh?.Invoke();
        }
    }

    public bool HoveringNPC => hoveredNPC != null;
    public bool PlacingNPC => heldNPC != null;
    public bool PickingNPC => pickNPC != null;

    private NPC hoveredNPC;
    private NPC heldNPC;
    private Action<NPC> pickNPC;

    private void FrameEditorUpdateNPC(NPCView view)
    {
        FrameEditorUpdate(view.config.GetFrame(frame), () => view.Refresh());

        if (Input.GetKeyDown(KeyCode.Minus))
        {
            var frames = view.config.frames;

            if (frames.Count > 1)
            {
                frames.RemoveAt(frame % frames.Count);

                SetFrame(frame);
            }

            view.Refresh();
        }
    }

    private void FrameEditorUpdateSurface(BlockView view, string surface)
    {
        FrameEditorUpdate(view.config.type.GetFrame(surface, frame), () => view.config.type.SetFrame(frame));

        if (Input.GetKeyDown(KeyCode.Minus))
        {
            var frames = view.config.type.textureMapping[surface];

            if (frames.Count > 1)
            {
                frames.RemoveAt(frame % frames.Count);

                SetFrame(frame);
            }

            view.Refresh();
        }
    }

    private OrientedTileEntry GetSelectedEntry()
    {
        return selectedSurfaceBlock.config.type.GetFrame(selectedSurfaceSurface, frame);
    }

    public void SelectedSurfaceRotate()
    {
        var entry = GetSelectedEntry();
        entry.orientation = entry.orientation.RotatedCW1();
        selectedSurfaceBlock.config.type.SetFrame(frame);
    }

    public void SelectedSurfaceDraw()
    {
        tileEditor.SetCanvas(GetSelectedEntry().sprite, context.palette);
        tileEditor.gameObject.SetActive(true);
    }

    public void SelectedSurfaceFlipH()
    {
        FrameFlipH(GetSelectedEntry());
        selectedSurfaceBlock.config.type.SetFrame(frame);
    }

    public void SelectedSurfaceFlipV()
    {
        FrameFlipV(GetSelectedEntry());
        selectedSurfaceBlock.config.type.SetFrame(frame);
    }

    private void NPCEditor(NPCView view)
    {
        outline.gameObject.SetActive(true);
        MoveOutline(view);

        FrameEditorUpdateNPC(view);

        hud.SetHotkeys("SURFACE (CHARACTER)\n, - edit frame\n. - replace frame\n+ - add frame\n- - remove frame\nr - rotate frame\nh/v - flip frame");

        if (Input.GetKeyDown(KeyCode.Equals))
        {
            tilePicker.Open(sprite =>
            {
                view.config.frames.Add(new OrientedTileEntry
                {
                    sprite = sprite,
                    orientation = SquareOrientation.None0,
                });
                view.Refresh();
            }, context.sprites.Skip(1).Take(8 * 8).ToList());
        }
    }

    private void SurfaceEditorUpdate()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, npcsMask))
        {
            var view = hit.collider.GetComponent<NPCView>();

            if (view != null)
            {
                hoveredNPC = view.config;

                NPCEditor(view);
            }

            return;
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            DeselectSurface();
        }

        if (selectedSurfaceBlock != null)
        {
            var view = selectedSurfaceBlock;
            var shape = selectedSurfaceSurface;

            outline.gameObject.SetActive(true);
            MoveOutline(view, shape);

            FrameEditorUpdateSurface(view, shape);

            hud.SetHotkeys($"SURFACE\nleft-click - select\nright-click - deselect");

            if (Input.GetKeyDown(KeyCode.Equals))
            {
                tilePicker.Open(sprite =>
                {
                    view.config.type.textureMapping[shape].Add(new OrientedTileEntry
                    {
                        sprite = sprite,
                        orientation = SquareOrientation.None0,
                    });
                    view.config.type.SetFrame(frame);
                }, context.sprites.Skip(1).Take(8 * 8).ToList());
            }
        }
        else if (Physics.Raycast(ray, out hit, Mathf.Infinity, collisionMask))
        {
            outline.gameObject.SetActive(true);

            var view = hit.transform.GetComponent<BlockView>();

            if (view == null)
                return;

            //cursor.transform.position = view.transform.position + hit.normal;

            var texture = view.block.texture;
            Vector2 pixel = hit.textureCoord;
            pixel.Scale(new Vector2(texture.width, texture.height));

            string shape = view.config.type.shape.GetShapeFromTriangle(hit.triangleIndex);

            MoveOutline(view, shape);

            FrameEditorUpdate(view.config.type.GetFrame(shape, frame), () => view.config.type.SetFrame(frame));

            hud.SetHotkeys("SURFACE\n, - edit frame\n. - replace frame\n+ - add frame\n- - remove frame\nr - rotate frame\nh/v - flip frame\n\nleft-click - select");

            if (Input.GetMouseButtonDown(0))
            {
                SelectSurface(view, shape);
            }

            if (Input.GetKeyDown(KeyCode.Equals))
            {
                tilePicker.Open(sprite =>
                {
                    view.config.type.textureMapping[shape].Add(new OrientedTileEntry
                    {
                        sprite = sprite,
                        orientation = SquareOrientation.None0,
                    });
                    view.config.type.SetFrame(frame);
                }, context.sprites.Skip(1).Take(8 * 8).ToList());
            }
        }
    }

    public IEnumerable<string> GetSavedRooms()
    {
        foreach (string folder in Directory.GetDirectories(Files.RoomsRoot))
        {
            string name = Path.GetFileName(folder);

            yield return name;
        }
    }

    [SerializeField]
    private GistSaveWindow gistWindow;

    public void DeleteNPC()
    {
        pickNPC = npc =>
        {
            room.data.npcs.Remove(npc);
        };
    }

    public void DuplicateNPC()
    {
        pickNPC = npc =>
        {
            heldNPC = new NPC
            {
                dialogues = npc.dialogues.ToList(),
                frames = npc.frames.ToList(),
            };
        };
    }

    public void CreateNPC()
    {
        heldNPC = new NPC
        {
            frames = new List<OrientedTileEntry> { new OrientedTileEntry { sprite = context.sprites[1] } },
        };
    }

    public void EditNPC()
    {
        pickNPC = characterEditor.Open;
    }

    public void TakeNPC()
    {
        pickNPC = npc =>
        {
            heldNPC = npc;
            room.data.npcs.Remove(npc);
        };
    }

    private void PickNPCEditor()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, npcsMask))
        {
            var npc = hit.collider.GetComponent<NPCView>();

            if (npc != null)
            {
                hoveredNPC = npc.config;

                hud.SetHotkeys("PICKING CHARACTER\nleft-click to pick this character\nright-click to cancel");
                MoveOutline(npc);

                if (Input.GetMouseButtonDown(0))
                {
                    pickNPC(npc.config);
                    pickNPC = null;
                }
            }
        }
        else
        {
            hud.SetHotkeys("PICKING CHARACTER\nleft-click a character to pick\nright-click to cancel");
        }

        if (Input.GetMouseButtonDown(1))
        {
            pickNPC = null;
        }
    }

    private void PlaceNPCEditor()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, blocksMask))
        {
            cursor.gameObject.SetActive(true);

            Vector3 position = hit.collider.transform.position;
            Vector3 surface = position + hit.normal;

            cursorPosition = position;
            cursor.localPosition = cursorPosition;
            cursor.transform.LookAt(surface);

            if (Input.GetMouseButtonDown(0))
            {
                heldNPC.position = surface;
                room.data.npcs.Add(heldNPC);
                heldNPC = null;
            }
        }

        hud.SetHotkeys("PLACING CHARACTER\nleft-click a block to place");
    }

    private void EditorUpdate()
    {
        if (Input.GetKeyDown(KeyCode.RightBracket))
        {
            SaveRoom(activeRoom ?? "test");
        }
        else if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            gistWindow.Open();
        }

        if (IsPointBlocked(Input.mousePosition) || characterEditor.gameObject.activeSelf)
            return;

        if (PickingNPC)
        {
            PickNPCEditor();

            return;
        }
        else if (PlacingNPC)
        {
            PlaceNPCEditor();

            return;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            surfaceMode = !surfaceMode;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            var labels = LabelledToggleData.FromStrings(GetSavedRooms().ToArray());

            labelPicker.Open(labels,
                             data => LoadRoom(data.label),
                             selected: labels.FirstOrDefault(l => l.label == activeRoom));
        }

        cursor.gameObject.SetActive(false);
        outline.gameObject.SetActive(false);

        if (surfaceMode)
        {
            SurfaceEditorUpdate();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Comma))
        {
            OpenBlockSelect();
        }

        if (!blockPicker.gameObject.activeInHierarchy)
        {
            blockPicker.PreviewBlock(types[blocki]);
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, blocksMask | npcsMask))
        {
            var view = hit.collider.GetComponent<NPCView>();

            if (view != null)
            {
                hoveredNPC = view.config;

                NPCEditor(view);

                return;
            }

            cursor.gameObject.SetActive(true);

            Vector3 position = hit.collider.transform.position;
            Vector3 surface = position + hit.normal;

            cursorPosition = position;
            cursor.localPosition = cursorPosition;
            cursor.transform.LookAt(surface);

            Block block = room.blocks.Find(b => b.position == cursorPosition);

            hud.SetHotkeys("BLOCK\nleft-click - place\nshift+QWEASD - rotate\ndel - remove\n. - copy\nz - toggle passability");

            if (Input.GetMouseButtonDown(0))
            {
                if (lastBlock == null)
                    lastBlock = room.blocks[0];

                room.blocks.Add(new Block
                {
                    position = surface,
                    rotation1 = lastBlock.rotation1,
                    rotation2 = lastBlock.rotation2,
                    rotation3 = lastBlock.rotation3,
                    type = types[blocki],
                });
            }
            else if (block != null && Input.GetKeyDown(KeyCode.Period))
            {
                blocki = types.IndexOf(block.type);
                lastBlock = block;
            }
            else if (block != null && Input.GetKeyDown(KeyCode.Z))
            {
                block.type.solid = !block.type.solid;
            }
            else if (block != null && Input.GetKeyDown(KeyCode.Delete))
            {
                room.blocks.Remove(block);
                block = null;
            }
            else if (block != null && Input.GetKey(KeyCode.LeftShift))
            {
                foreach (var pair in axes)
                {
                    if (Input.GetKeyDown(pair.Key))
                    {
                        Rotate(block, pair.Value, 1);

                        lastBlock = block;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                var type = new BlockType(block.type.shape);
                block.type = type;

                foreach (var entry in type.shape.shapes)
                {
                    type.TextureShape(entry.Key, context.sprites[1], SquareOrientation.None0);
                }

                types.Add(type);
            }
            else if (Input.GetKeyDown(KeyCode.Backslash))
            {   if (lastBlock == null)
                    lastBlock = room.blocks[0];

                room.blocks.Remove(block);
                room.blocks.Add(new Block
                {
                    position = cursorPosition,
                    rotation1 = lastBlock.rotation1,
                    rotation2 = lastBlock.rotation2,
                    rotation3 = lastBlock.rotation3,
                    type = types[blocki],
                });
            }
            else if (Input.GetKeyDown(KeyCode.V) && heldNPC != null)
            {
                heldNPC.position = surface;
                room.data.npcs.Add(heldNPC);
                heldNPC = null;
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                var npc = new NPC
                {
                    position = surface,
                    frames = new List<OrientedTileEntry>
                    { new OrientedTileEntry { sprite = context.sprites[1] } },
                };

                room.data.npcs.Add(npc);
            }
        }
        else
        {
            cursor.gameObject.SetActive(false);
        }
    }

    private static Vector2[] coords = new Vector2[]
    {
        Vector2.down + Vector2.left,
        Vector2.down + Vector2.right,
        Vector2.up   + Vector2.right,
        Vector2.up   + Vector2.left,
    };

    private void MoveOutline(NPCView npc)
    {
        outline.gameObject.SetActive(true);
        outline.transform.SetPositionAndRotation(npc.transform.position, 
                                                 npc.transform.rotation);
        
        outline.positionCount = 4;

        for (int i = 0; i < 4; ++i)
        { 
            outline.SetPosition(i, coords[i] * 0.5f);
        }
    }

    public bool SurfaceIsSelected => selectedSurfaceBlock != null;
    private BlockView selectedSurfaceBlock;
    private string selectedSurfaceSurface;

    private void SelectSurface(BlockView block, string surface)
    {
        selectedSurfaceBlock = block;
        selectedSurfaceSurface = surface;

        MoveOutline(block, surface);
    }

    private void DeselectSurface()
    {
        selectedSurfaceBlock = null;
        selectedSurfaceSurface = null;
    }

    private void MoveOutline(BlockView block, string surface)
    {
        outline.transform.SetPositionAndRotation(block.transform.position, 
                                                 block.transform.rotation);

        var shape = block.config.type.shape;
        var points = shape.shapes[surface].Select(i => shape.vertices[i]).Distinct().ToList();

        outline.positionCount = points.Count;

        for (int i = 0; i < outline.positionCount; ++i)
        {
            outline.SetPosition(i, points[i]);
        }
    }

    public bool surfaceMode;

    private void SetFrame(int frame)
    {
        foreach (var block in types)
        {
            block.SetFrame(frame);
        }

        world.npcs.MapActive(view => view.SetFrame(frame));
    }

    public void NextFrame()
    {
        frame += 1;

        SetFrame(frame);
    }

    public void PrevFrame()
    {
        frame += 1;

        SetFrame(frame);
    }

    public void RemoveSelectedFrame()
    {
        var frames = selectedSurfaceBlock.block.textureMapping[selectedSurfaceSurface];

        if (frames.Count > 1)
        {
            frames.RemoveAt(frame % frames.Count);

            SetFrame(frame);
        }
    }

    public void AddFrameToSelected()
    {
        var frames = selectedSurfaceBlock.block.textureMapping[selectedSurfaceSurface];

        tilePicker.Open(sprite =>
        {
            frames.Insert(frame % frames.Count + 1, new OrientedTileEntry
            {
                sprite = sprite,
                orientation = SquareOrientation.None0,
            });

            NextFrame();
        }, context.sprites.Skip(1).Take(8 * 8).ToList());
    }

    public void ReplaceFrameOfSelected()
    {
        tilePicker.Open(sprite =>
        {
            GetSelectedEntry().sprite = sprite;
            selectedSurfaceBlock.config.type.SetFrame(frame);
        }, context.sprites.Skip(1).Take(8 * 8).ToList());
    }

    public int frame;
    private IEnumerator FakeAnimation()
    {
        while (true)
        {
            yield return new WaitForSeconds(.25f);

            if (selectedSurfaceBlock == null)
            {
                NextFrame();
            }
        }
    }

    public void EnterPlaying()
    {
        Playing = true;
    }

    public void EnterEditing()
    {
        Playing = false;
    }

    private void Update()
    {
        if (room.blocks == null)
            return;

        bool blocked = IsPointBlocked(Input.mousePosition);
        bool halt = !Input.GetMouseButton(1) || blocked;
        mouselook1.enabled = !halt;
        mouselook2.enabled = !halt;

        cursor.gameObject.SetActive(!blocked && !Playing);

        if (!blocked && Input.GetKeyDown(KeyCode.Slash))
        {
            StartCoroutine(RandomisePalette(context.palette));
        }

        if (!blocked && Input.GetKeyDown(KeyCode.P))
        {
            Playing = !Playing;
        }

        if (!Playing)
        {
            hud.SetHotkeys();

            EditorUpdate();
        }
        else
        {
            outline.gameObject.SetActive(false);

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            hud.SetHotkeys();

            if (Physics.Raycast(ray, out hit, 7.5f, npcsMask))
            {
                var view = hit.collider.GetComponent<NPCView>();

                if (view != null)
                {
                    hoveredNPC = view.config;

                    hud.SetHotkeys("CHARACTER\nleft-click - interact");

                    if (Input.GetMouseButtonDown(0))
                    {
                        view.StopAllCoroutines();
                        view.StartCoroutine(view.ShowMessage());
                    }
                }
            }
        }

        world.SetConfig(room);
    }

    private BlockTypeData BlockTypeToData(BlockType type)
    {
        foreach (var entry in type.textureMapping.Values)
        {
            context.FixReferences(entry);
        }

        return new BlockTypeData
        {
            name = Guid.NewGuid().ToString(),
            shape = shapeNames[type.shape],
            solid = type.solid,
            texturing = type.textureMapping.Select(pair => new BlockTypeData.TextureEntry
            {
                surface = pair.Key,
                frames = pair.Value,
            }).ToList(),
        };
    }

    private BlockType BlockTypeFromData(BlockTypeData data)
    {
        var type = new BlockType(shapes[data.shape]);
        type.solid = data.solid;

        foreach (var entry in data.texturing)
        {
            context.FixReferences(entry.frames);

            type.texture = context.texture;
            type.textureMapping[entry.surface] = entry.frames;
            type.SetFrame(0);
        }

        return type;
    }

    private RoomData PreSave()
    {
        var type2data = room.blocks.Select(b => b.type)
                              .Distinct()
                              .ToDictionary(t => t, t => BlockTypeToData(t));

        foreach (var sprite in context.sprites)
        {
            sprite.Pad();
        }

        context.texture.Apply();

        room.data.npcs.ForEach(npc => npc.frames.ForEach(frame => frame.tile = context.SpriteToIndex(frame.sprite)));

        var roomdata = new RoomData
        {
            palette = context.palette,
            npcs = room.data.npcs,
            blockTypes = type2data.Values.ToList(),
            blocks = room.blocks.Select(b => new BlockData
            {
                position = b.position,
                rotation = new IntVector3(b.rotation1, b.rotation2, b.rotation3),
                shape = type2data[b.type].name,
            }).ToList(),
        };

        context.palette.WriteToTexture(context.texture);
        context.texture.Apply();

        return roomdata;
    }

    private string ObjectToBase64<T>(T value)
    {
        return Convert.ToBase64String(Encoding.UTF8.GetBytes(JSON.Serialize(value)));
    }

    public void GistRoom(Action<string> OnComplete, Action<float> OnProgress = null)
    {
        loadingContainer.SetActive(true);

        var roomdata = PreSave();
        var bytes = context.texture.uTexture.EncodeToPNG();

        StartCoroutine(Gist.Create("sketch-blocks room",
                       new Dictionary<string, string>
                       {
                           ["room.json"] = ObjectToBase64(roomdata),
                           ["texture.png"] = Convert.ToBase64String(bytes),
                       }, id => 
        {
            loadingContainer.SetActive(false);

            UpdateSearchWithID(id);
            OnComplete(id);
        }, progress => progressBar.value = progress));
    }

    [SerializeField]
    private GameObject loadingContainer;
    [SerializeField]
    private Slider progressBar;

    public void Ungist(string id, Action OnComplete = null, Action<float> OnProgress = null)
    {
        loadingContainer.SetActive(true);

        StartCoroutine(Gist.Download(id, files =>
        { 
            var room = JSON.Deserialize<RoomData>(Encoding.UTF8.GetString(Convert.FromBase64String(files["room.json"])));
            byte[] tex = Convert.FromBase64String(files["texture.png"]);

            LoadRoom(room, tex);

            loadingContainer.SetActive(false);

            OnComplete();
        }, this, progress => progressBar.value = progress));
    }

    public void SaveRoom(string name)
    {
        var roomdata = PreSave();

        Files.Save($"{Files.RoomsRoot}/{name}/room.json", roomdata);
        Files.Save($"{Files.RoomsRoot}/{name}/texture.png", context.texture.uTexture.EncodeToPNG());
    }

    private string activeRoom;

    private void CorrectTexture()
    {
        var copy = new TextureByte(2048, 2048);
        copy.pixels = context.texture.pixels.ToArray();

        for (int y = 59; y >= 0; --y)
        {
            for (int x = 59; x >= 0; --x)
            {
                var src = copy.GetSprite(new IntRect(x * 32, y * 32, 32, 32), IntVector2.zero);
                var dst = context.texture.GetSprite(new IntRect(x * 34 + 1, y * 34 + 1, 32, 32), IntVector2.zero);

                dst.Blend(src, TextureByte.replace);
            }
        }

        context.texture.Apply();
    }

    private void FixNPC(NPC npc)
    {
        if (npc.frames.Count == 0)
        {
            npc.frames.Add(new OrientedTileEntry
            {
                orientation = SquareOrientation.None0,
                sprite = context.sprites[1],
                tile = 0,
            });
        }
    }

    public void LoadRoom(RoomData roomdata, byte[] texturebytes)
    {
        activeRoom = name;

        room = new Room
        {
            blocks = new List<Block>(),
            data = roomdata,
        };

        roomdata.npcs.ForEach(FixNPC);
        roomdata.npcs.ForEach(npc => npc.frames?.ForEach(frame => frame.sprite = context.IndexToSprite(frame.tile)));

        var types = roomdata.blockTypes.ToDictionary(d => d.name, d => BlockTypeFromData(d));
        this.types = this.types.Concat(types.Values).ToList();

        room.blocks = roomdata.blocks.Select(data => new Block
        {
            position = data.position,
            rotation1 = data.rotation.x,
            rotation2 = data.rotation.y,
            rotation3 = data.rotation.z,
            type = types[data.shape],
        }).ToList();

        context.texture.DecodeFromPNG(texturebytes);//, palette);
        context.palette = roomdata.palette;
        context.palette.texture = paletteTexture;

        context.palette.ReadFromTexture(context.texture);
        context.palette.Apply();
    }

    public void LoadRoom(string name)
    {
        RoomData roomdata;

#if !UNITY_WEBGL
        if (Files.Load($"{Files.RoomsRoot}/{name}/room.json", out roomdata))
        {
            byte[] bytes;

            Files.Load($"{Files.RoomsRoot}/{name}/texture.png", out bytes);

            LoadRoom(roomdata, bytes);
        }
        else 
#elif !UNITY_EDITOR
        bool success = false;
        
        try
        {
            string id = GetWindowSearch().Split('=')[1];

            if (id.Length > 4)
            {
                Ungist(id);
                success = true;
            }
        }
        catch (System.Exception e)
        {
        }

        if (!success)
#endif
        {
            room = new Room
            {
                data = new RoomData(),
                blocks = new List<Block>(),
            };

            StartCoroutine(RandomisePalette(context.palette));

            for (int y = -10; y < 10; ++y)
                for (int x = -10; x < 10; ++x)
                {
                    var type = types[Random.Range(0, types.Count)];

                    room.blocks.Add(new Block
                    {
                        position = new IntVector3(x, -Mathf.PerlinNoise(x * 0.1f, y * 0.1f) * 10, y),
                        rotation1 = Random.Range(0, 4),
                        rotation2 = Random.Range(0, 4),
                        type = type,
                    });
                }
        }

        context.texture.Apply();
    }
}
