﻿using UnityEngine;

public enum SquareOrientation
{
    None0,
    None1,
    None2,
    None3,
    Flip0,
    Flip1,
    Flip2,
    Flip3,
}

public static partial class TextureOrientationExtensions
{
    public static SquareOrientation FlippedH(this SquareOrientation orientation)
    {
        switch (orientation)
        {
        case SquareOrientation.None0: return SquareOrientation.Flip0;
        case SquareOrientation.None1: return SquareOrientation.Flip3;
        case SquareOrientation.None2: return SquareOrientation.Flip2;
        case SquareOrientation.None3: return SquareOrientation.Flip1;
        case SquareOrientation.Flip0: return SquareOrientation.None0;
        case SquareOrientation.Flip1: return SquareOrientation.None3;
        case SquareOrientation.Flip2: return SquareOrientation.None2;
        case SquareOrientation.Flip3: return SquareOrientation.None1;
        default: return SquareOrientation.None0;
        }
    }

    public static SquareOrientation FlippedV(this SquareOrientation orientation)
    {
        switch (orientation)
        {
        case SquareOrientation.None0: return SquareOrientation.Flip2;
        case SquareOrientation.None1: return SquareOrientation.Flip1;
        case SquareOrientation.None2: return SquareOrientation.Flip0;
        case SquareOrientation.None3: return SquareOrientation.Flip3;
        case SquareOrientation.Flip0: return SquareOrientation.None2;
        case SquareOrientation.Flip1: return SquareOrientation.None1;
        case SquareOrientation.Flip2: return SquareOrientation.None0;
        case SquareOrientation.Flip3: return SquareOrientation.None3;
        default: return SquareOrientation.None0;
        }
    }

    public static SquareOrientation RotatedCW1(this SquareOrientation orientation)
    {
        switch (orientation)
        {
        case SquareOrientation.None0: return SquareOrientation.None1;
        case SquareOrientation.None1: return SquareOrientation.None2;
        case SquareOrientation.None2: return SquareOrientation.None3;
        case SquareOrientation.None3: return SquareOrientation.None0;
        case SquareOrientation.Flip0: return SquareOrientation.Flip1;
        case SquareOrientation.Flip1: return SquareOrientation.Flip2;
        case SquareOrientation.Flip2: return SquareOrientation.Flip3;
        case SquareOrientation.Flip3: return SquareOrientation.Flip0;
        default: return SquareOrientation.None0;
        }
    }

    public static SquareOrientation RotatedACW1(this SquareOrientation orientation)
    {
        switch (orientation)
        {
        case SquareOrientation.None0: return SquareOrientation.None3;
        case SquareOrientation.None1: return SquareOrientation.None0;
        case SquareOrientation.None2: return SquareOrientation.None1;
        case SquareOrientation.None3: return SquareOrientation.None2;
        case SquareOrientation.Flip0: return SquareOrientation.Flip3;
        case SquareOrientation.Flip1: return SquareOrientation.Flip0;
        case SquareOrientation.Flip2: return SquareOrientation.Flip1;
        case SquareOrientation.Flip3: return SquareOrientation.Flip2;
        default: return SquareOrientation.None0;
        }
    }

    private static Vector2 Flipped(Vector2 coord)
    {
        coord.x = 1 - coord.x;
        return coord;
    }

    private static Vector2 RotatedCW(Vector2 coord)
    {
        return new Vector2(1 - coord.y, coord.x);
    }

    private static Vector2 RotatedACW(Vector2 coord)
    {
        return new Vector2(coord.y, 1 - coord.x);
    }

    public static Vector2 Transform(this SquareOrientation orientation, Vector2 coord)
    {
        switch (orientation)
        {
        case SquareOrientation.None0: return new Vector2(    coord.x,     coord.y);
        case SquareOrientation.None1: return new Vector2(1 - coord.y,     coord.x);
        case SquareOrientation.None2: return new Vector2(1 - coord.x, 1 - coord.y);
        case SquareOrientation.None3: return new Vector2(    coord.y, 1 - coord.x);
        case SquareOrientation.Flip0: return new Vector2(1 - coord.x,     coord.y);
        case SquareOrientation.Flip1: return new Vector2(    coord.y,     coord.x);
        case SquareOrientation.Flip2: return new Vector2(    coord.x, 1 - coord.y);
        case SquareOrientation.Flip3: return new Vector2(1 - coord.y, 1 - coord.x);
        default: return coord;
        }
    }
}
