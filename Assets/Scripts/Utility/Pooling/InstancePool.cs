﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.Profiling;

public abstract class InstancePoolBase<TConfig, TInstance>
    where TInstance : IConfigView<TConfig>
{    
    protected Dictionary<TConfig, TInstance> instances
        = new Dictionary<TConfig, TInstance>();
    protected List<TInstance> spare = new List<TInstance>();

    protected abstract TInstance CreateNew();

    protected TInstance FindNew(TConfig config)
    {
        TInstance instance;

        if (spare.Count > 0)
        {
            instance = spare[spare.Count - 1];
            spare.RemoveAt(spare.Count - 1);
        }
        else
        {
            instance = CreateNew();
        }

        Configure(config, instance);

        return instance;
    }

    public TInstance Get(TConfig config)
    {
        TInstance instance;

        if (!instances.TryGetValue(config, out instance))
        {
            instance = FindNew(config);
        }

        return instance;
    }

    public bool Discard(TConfig config)
    {
        TInstance instance;

        if (instances.TryGetValue(config, out instance))
        {
            instances.Remove(config);
            spare.Add(instance);

            Cleanup(config, instance);

            return true;
        }

        return false;
    }

    public void Clear()
    {
        foreach (var pair in instances)
        {
            Cleanup(pair.Key, pair.Value);
        }

        spare.AddRange(instances.Values);
        instances.Clear();
    }

    protected virtual void Configure(TConfig config, TInstance instance)
    {
        instances.Add(config, instance);
        instance.SetConfig(config);
    }

    protected virtual void Cleanup(TConfig config, TInstance instance)
    {
        instance.Cleanup();
    }

    public void Refresh()
    {
        MapActive(i => i.Refresh());
    }

    public void SetActive(params TConfig[] configs)
    {
        SetActive(configs as IEnumerable<TConfig>);
    }

    private HashSet<TConfig> prev = new HashSet<TConfig>();
    private HashSet<TConfig> next = new HashSet<TConfig>();
    private List<TConfig> added = new List<TConfig>();

    private bool SetActiveIList(IList<TConfig> active)
    {
        if (active == null) return false;

        Profiler.BeginSample("InstancePoolBase.SetActiveIList");

        int count = active.Count;

        // work out which shortcuts should be active next, removing them from the
        // previous set of shortcuts to indicate they don't need to be discarded
        for (int i = 0; i < count; ++i)
        {
            TConfig shortcut = active[i];

            // if a shortcut couldn't be removed from the previous shortcuts then it
            // was not present and so needs to be added (but 
            if (!prev.Remove(shortcut))
            {
                added.Add(shortcut);
            }

            next.Add(shortcut);
        }

        // every shortcut from the previous set that was not removed must not be in
        // the next set and so needs to be discarded
        foreach (var shortcut in prev)
        {
            Discard(shortcut);
        }

        prev.Clear();

        // now we've freed up the unused instances, activate all the newly added
        // elements
        foreach (var shortcut in added)
        {
            FindNew(shortcut);
        }

        added.Clear();

        // the next set becomes the previous set
        var temp = prev;
        prev = next;
        next = temp;

        Profiler.EndSample();

        return true;
    }

    public virtual void SetActive(IEnumerable<TConfig> active)
    {
        if (SetActiveIList(active as IList<TConfig>)) return;

        Profiler.BeginSample("InstancePoolBase.SetActive");

        // work out which shortcuts should be active next, removing them from the
        // previous set of shortcuts to indicate they don't need to be discarded
        foreach (var shortcut in active)
        {
            // if a shortcut couldn't be removed from the previous shortcuts then it
            // was not present and so needs to be added
            if (!prev.Remove(shortcut))
            {
                added.Add(shortcut);
            }

            next.Add(shortcut);
        }

        // every shortcut from the previous set that was not removed must not be in
        // the next set and so needs to be discarded
        foreach (var shortcut in prev)
        {
            Discard(shortcut);
        }

        prev.Clear();

        // now we've freed up the unused instances, activate all the newly added
        // elements
        foreach (var shortcut in added)
        {
            FindNew(shortcut);
        }

        added.Clear();

        // the next set becomes the previous set
        var temp = prev;
        prev = next;
        next = temp;

        Profiler.EndSample();
    }

    public void MapActive(Action<TInstance> action)
    {
        foreach (TInstance instance in instances.Values)
        {
            action(instance);
        }
    }

    public bool IsActive(TConfig shortcut)
    {
        return instances.ContainsKey(shortcut);
    }

    public bool DoIfActive(TConfig shortcut, 
                           Action<TInstance> action)
    {
        TInstance instance;

        if (instances.TryGetValue(shortcut, out instance))
        {
            action(instance);

            return true;
        }
        else
        {
            return false;
        }
    }
}

[Serializable]
public class InstancePoolSetup
{
    public GameObject prefab;
    public Transform parent;

    private TInstance GetConcretePrefab<TInstance>()
        where TInstance : class
    {
        var prefab = this.prefab.GetComponent<TInstance>();

        Assert.IsNotNull(prefab, string.Format("Prefab {0} is missing {1} component", 
                                               this.prefab.name,
                                               typeof(TInstance).Name));

        return prefab;
    }

    public void Finalise<TInstance>(ref IndexedPool<TInstance> pool)
        where TInstance : Component => pool = Finalise<TInstance>();
    public void Finalise<TConfig>(ref InstancePool<TConfig> pool, bool sort = true) => pool = Finalise<TConfig>(sort);
    public void Finalise<TConfig, TInstance>(ref InstancePool<TConfig, TInstance> pool, bool sort = true) 
        where TInstance : InstanceView<TConfig> => pool = Finalise<TConfig, TInstance>(sort);

    public IndexedPool<TInstance> Finalise<TInstance>()
        where TInstance : Component
    {
        var prefab = GetConcretePrefab<TInstance>();

        return new IndexedPool<TInstance>(prefab, parent);
    }

    public InstancePool<TConfig> Finalise<TConfig>(bool sort=true)
    {
        var prefab = GetConcretePrefab<InstanceView<TConfig>>();

        return new InstancePool<TConfig>(prefab, parent, sort: sort);
    }

    public InstancePool<TConfig, TInstance> Finalise<TConfig, TInstance>(bool sort=true)
        where TInstance : InstanceView<TConfig>
    {
        var prefab = GetConcretePrefab<TInstance>();

        return new InstancePool<TConfig, TInstance>(prefab, parent, sort: sort);
    }
}

public class InstancePool<TConfig, TInstance> : InstancePoolBase<TConfig, TInstance>
    where TInstance : InstanceView<TConfig>
{
    protected TInstance prefab;
    protected Transform parent;
    protected bool sort;

    public InstancePool(TInstance prefab, 
                        Transform parent, 
                        bool sort=true)
    {
        this.prefab = prefab;
        this.parent = parent;
        this.sort = sort;
    }

    protected override TInstance CreateNew()
    {
        TInstance instance = UnityEngine.Object.Instantiate(prefab);

        instance.Setup();

        return instance;
    }

    protected override void Configure(TConfig config, TInstance instance)
    {
        instance.transform.SetParent(parent, false);
        instance.gameObject.SetActive(true);

        base.Configure(config, instance);
    }

    protected override void Cleanup(TConfig config, TInstance instance)
    {
        instance.gameObject.SetActive(false);

        base.Cleanup(config, instance);
    }

    public override void SetActive(IEnumerable<TConfig> active)
    {
        base.SetActive(active);

        if (sort)
        {
            foreach (TConfig shortcut in active)
            {
                Get(shortcut).transform.SetAsLastSibling();
            }
        }
    }
}

public class InstancePool<TConfig> : InstancePool<TConfig, InstanceView<TConfig>>
{
    public InstancePool(InstanceView<TConfig> prefab, Transform parent, bool sort=true)
        : base(prefab, parent, sort)
    {
    }
}

