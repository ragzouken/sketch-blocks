﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class Billboard : MonoBehaviour 
{
    [SerializeField]
    new private Camera camera;
    [SerializeField]
    private bool lockVertical;

    private void Update()
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        if (lockVertical)
        {
            transform.rotation = Quaternion.AngleAxis(camera.transform.rotation.eulerAngles.y, Vector3.up);
        }
        else
        {
            transform.LookAt(camera.transform.position, camera.transform.up);
        }
    }
}
