﻿using UnityEngine;
using System;

public static class JSON
{
    static JSON()
    {
        //LitJson.UnityTypeBindings.Register();

        LitJson.JsonMapper.RegisterExporter<float>((obj, writer) => writer.Write(Convert.ToDouble(obj)));
        LitJson.JsonMapper.RegisterImporter<double, float>(input => Convert.ToSingle(input));

        LitJson.JsonMapper.RegisterExporter<IntVector3>((value, writer) => 
        {
            writer.WriteObjectStart();
            writer.WritePropertyName("x");
            writer.Write(value.x);
            writer.WritePropertyName("y");
            writer.Write(value.y);
            writer.WritePropertyName("z");
            writer.Write(value.z);
            writer.WriteObjectEnd();
		});
    }

    public static string Serialize<T>(T value)
    {
        //return LitJson.JsonMapper.ToJson(value);
        return JsonUtility.ToJson(value);
    }

    public static T Deserialize<T>(string json)
    {
        //return LitJson.JsonMapper.ToObject<T>(json);
        return JsonUtility.FromJson<T>(json);
    }

    public static T Copy<T>(T value)
    {
        return Deserialize<T>(Serialize(value));
    }
}
