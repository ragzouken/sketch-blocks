﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Exception = System.Exception;
using System.IO;

public static class Files
{
    public static string SaveRoot => Application.persistentDataPath;
    public static string SettingsRoot => $"{SaveRoot}/settings";
    public static string RoomsRoot => $"{SaveRoot}/rooms";
    public static string ShapesRoot => $"{SaveRoot}/shapes";

    public static string DefaultShapesRoot => $"{Application.streamingAssetsPath}/shapes/";

    /// <summary>
    /// Save text to given path, return whether or not this was successful. 
    /// Swallows exceptions but logs them.
    /// </summary>
    public static bool Save(string path, string text)
    {
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            File.WriteAllText(path, text);

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't save text to '{path}'");

            return false;
        }
    }

    /// <summary>
    /// Attempt to load text from the given path, returning whether this was
    /// successful or not. Swallows exceptions but logs them.
    /// </summary>
    public static bool Load(string path, out string text)
    {
        try
        {
            text = File.ReadAllText(path);

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't load text from '{path}'");
            text = default(string);

            return false;
        }
    }

    /// <summary>
    /// Save bytes to given path, return whether or not this was successful. 
    /// Swallows exceptions but logs them.
    /// </summary>
    public static bool Save(string path, byte[] bytes)
    {
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            File.WriteAllBytes(path, bytes);

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't save bytes to '{path}'");

            return false;
        }
    }

    /// <summary>
    /// Attempt to load bytes from the given path, returning whether this was
    /// successful or not. Swallows exceptions but logs them.
    /// </summary>
    public static bool Load(string path, out byte[] bytes)
    {
        try
        {
            bytes = File.ReadAllBytes(path);

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't load bytes from '{path}'");
            bytes = default(byte[]);

            return false;
        }
    }

    /// <summary>
    /// Serialize data to JSON and save to given path, return whether or not
    /// this was successful. Swallows exceptions but logs them.
    /// </summary>
    public static bool Save<TData>(string path, TData data)
    {
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            File.WriteAllText(path, JSON.Serialize(data));

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't save {typeof(TData).FullName} to '{path}'");

            return false;
        }
    }

    /// <summary>
    /// Attempt to deserialize JSON data from the given path, returning whether 
    /// this was successful or not. Swallows exceptions but logs them.
    /// </summary>
    public static bool Load<TData>(string path, out TData data)
    {
        try
        {
            string json = File.ReadAllText(path);
            data = JSON.Deserialize<TData>(json);

            return true;
        }
        catch (Exception exception)
        {
            Debug.LogException(exception);
            Debug.LogError($"[Files] Couldn't load {typeof(TData).FullName} from '{path}'");
            data = default(TData);

            return false;
        }
    }
}
