﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public static partial class LINQExtensions
{
    /// Output a line of debug for each item in a LINQ
    public static IEnumerable<T> Debug<T>(this IEnumerable<T> sequence,
                                          Func<T, string> output=null,
                                          string header="LINQ Debug")
    {
        var items = sequence.ToArray();
        
        UnityEngine.Debug.Log(items.ToLines(output, header));

        return items;
    }

    public static string ToLine<T>(this IEnumerable<T> sequence,
                                   Func<T, string> output = null,
                                   string delimeter=", ")
    {
        output = output ?? (item => item.ToString());

        var elements = sequence.Select(item => output(item)).ToArray();

        return string.Join(delimeter, elements);
    }
    
    public static string ToLines<T>(this IEnumerable<T> sequence,
                                    Func<T, string> output = null,
                                    string header = null)
    {
        var builder = new System.Text.StringBuilder();
        if (header != null) builder.AppendLine(header);

        output = output ?? (item => item.ToString());

        foreach (var item in sequence)
        {
            builder.AppendLine(output(item));
        }

        return builder.ToString();
    }

    public static IEnumerable<string> ToStrings<T>(this IEnumerable<T> sequence,
                                                   Func<T, string> output = null)
    {
        output = output ?? (item => item.ToString());

        return sequence.Select(item => output(item));
    }

    public static string ToJoined<T>(this IEnumerable<T> sequence,
                                     string separator = ", ",
                                     Func<T, string> output = null)
    {
        output = output ?? (item => item.ToString());

        return string.Join(separator, sequence.Select(item => output(item)).ToArray());
    }

    public static T RandomElement<T>(this IEnumerable<T> sequence)
    {
        int count = sequence.Count();

        Assert.IsTrue(count > 0, "Can't take random element from empty sequence!");

        return sequence.ElementAt(Random.Range(0, count));
    }

    public static T MaxBy<T>(this IEnumerable<T> sequence, 
                             Func<T, float> Value)
    {
        float maxValue = Mathf.NegativeInfinity;
        T maxItem = default(T);

        foreach (T item in sequence)
        {
            float value = Value(item);

            if (value > maxValue)
            {
                maxValue = value;
                maxItem = item;
            }
        }

        return maxItem;
    }

    public static T MinBy<T>(this IEnumerable<T> sequence,
                             Func<T, float> Value)
    {
        float minValue = Mathf.Infinity;
        T minItem = default(T);

        foreach (T item in sequence)
        {
            float value = Value(item);

            if (value < minValue)
            {
                minValue = value;
                minItem = item;
            }
        }

        return minItem;
    }

    public static T ElementAtCycled<T>(this IEnumerable<T> sequence,
                                       int index)
    {
        int length = sequence.Count();

        while (index < 0) index += length;
        
        return sequence.ElementAt(index % length);
    }

    public static T NextCycled<T>(this IEnumerable<T> sequence, T current)
    {
        int offset = sequence.TakeWhile(item => !item.Equals(current)).Count();
        var skipped = sequence.Skip(offset + 1);

        if (skipped.Any())
        {
            return skipped.First();
        }
        else
        {
            return sequence.First();
        }
    }

    public static T PrevCycled<T>(this IEnumerable<T> sequence, T current)
    {
        int offset = sequence.TakeWhile(item => !item.Equals(current)).Count();

        if (offset == 0)
        {
            return sequence.Last();
        }
        else
        {
            return sequence.Skip(offset - 1).First();
        }
    }

    public static IEnumerator ToListCO<T>(this IEnumerable<T> sequence,
                                          Action<List<T>> OnComplete,
                                          float limit = 0)
    {
        float checkpoint = Time.realtimeSinceStartup;

        var list = new List<T>();

        foreach (T item in sequence)
        {
            list.Add(item);

            if (Time.realtimeSinceStartup - checkpoint >= limit)
            {
                yield return null;

                checkpoint = Time.realtimeSinceStartup;
            }
        }

        OnComplete(list);
    }

    public static IEnumerator ToArrayCO<T>(this IEnumerable<T> sequence,
                                           Action<T[]> OnComplete,
                                           float limit=0)
    {
        float checkpoint = Time.realtimeSinceStartup;

        var list = new List<T>();

        foreach (T item in sequence)
        {
            list.Add(item);

            if (Time.realtimeSinceStartup - checkpoint >= limit)
            {
                yield return null;

                checkpoint = Time.realtimeSinceStartup;
            }
        }

        OnComplete(list.ToArray());
    }

    public static IEnumerable<T> WithComponent<T>(this IEnumerable<GameObject> sequence)
        where T : Component
    {
        return sequence.Select(go => go.GetComponent<T>()).OfType<T>();
    }

    public static T[] Appended<T>(this T[] sequence, T item)
    {
        return sequence.Concat(new[] { item }).ToArray();
    }
}
