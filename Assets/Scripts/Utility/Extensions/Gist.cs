﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.Text;
using System.Linq;

public static class Gist
{
    public static IEnumerator Create(string description,
                                     Dictionary<string, string> files,
                                     Action<string> onComplete,
                                     Action<float> OnProgress = null)
    {
        var fileString = new StringBuilder();

        fileString.AppendLine("{");

        bool first = true;

        foreach (var file in files)
        {
            if (!first) fileString.AppendLine(",");                        

            fileString.Append($"\"{file.Key}\": {{ \"content\": \"{file.Value}\" }}");

            if (first) first = false;
        }

        fileString.AppendLine();
        fileString.AppendLine("}");

        var encoding = new UTF8Encoding();
        var jsonBuilder = new StringBuilder();
        jsonBuilder.Append($@"{{ ""description"":""{description}"", ""public"":true, ""files"": {fileString.ToString()} }}");

        var request = new WWW("https://api.github.com/gists", 
                                encoding.GetBytes(jsonBuilder.ToString()));

        //Debug.Log(jsonBuilder.ToString());

        while (!request.isDone)
        {
            OnProgress?.Invoke(request.uploadProgress);

            yield return null;
        }

        yield return request;

        var response = LitJson.JsonMapper.ToObject(request.text);

        onComplete((string) response["id"]);
    }

    public static IEnumerator Untruncate(string url, 
                                         Action<string> onComplete,
                                         Action<float> OnProgress = null)
    {
        var request = new WWW(url);

        while (!request.isDone)
        {
            OnProgress?.Invoke(request.progress);

            yield return null;
        }

        onComplete(request.text);
    }

    public static IEnumerator Download(string id, 
                                       Action<Dictionary<string, string>> onComplete,
                                       MonoBehaviour parent=null,
                                       Action<float> OnProgress = null)
    {
        var files = new Dictionary<string, string>();
        var request = new WWW($"https://api.github.com/gists/{id}");

        while (!request.isDone)
        {
            OnProgress?.Invoke(request.progress);

            yield return new WaitForSeconds(1f);
        }

        Files.Save("../response.txt", request.text);

        UnityEngine.Profiling.Profiler.BeginSample("Parse Gist JSON");

        var response = LitJson.JsonMapper.ToObject(new LitJson.JsonReader(request.text));

        UnityEngine.Profiling.Profiler.EndSample();

        var filenames = response["files"].Keys.ToList();
        var truncated = filenames.Where(name => (bool) response["files"][name]["truncated"]).ToList();
        var progresses = response["files"].Keys.ToDictionary(name => name, name => ((bool) response["files"][name]["truncated"]) ? 0f : 1f);

        foreach (var file in filenames)
        {
            if ((bool) response["files"][file]["truncated"])
            {
                string f = file;

                yield return parent.StartCoroutine(Untruncate((string) response["files"][file]["raw_url"], data =>
                {
                    files.Add(f, data);
                }, progress =>
                {
                    progresses[file] = progress;

                    OnProgress?.Invoke(progresses.Values.Sum() / progresses.Count);
                }));
            }
            else
            {
                files.Add(file, (string) response["files"][file]["content"]);
            }
        }

        onComplete(files);
    }
}
