﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Context
{
    public Palette palette;
    public TextureByte texture;
    public List<TextureByte.Sprite> sprites;

    public int SpriteToIndex(TextureByte.Sprite sprite)
    {
        return sprites.IndexOf(sprite);
    }

    public TextureByte.Sprite IndexToSprite(int index)
    {
        return sprites[index];
    }

    public void FixReferences(List<OrientedTileEntry> entries)
    {
        foreach (var entry in entries)
        {
            if (entry.sprite == null)
            {
                entry.sprite = IndexToSprite(entry.tile);
            }
            else
            {
                entry.tile = SpriteToIndex(entry.sprite);
            }
        }
    }
}

[Serializable]
public class Room
{
    public List<Block> blocks;
    public RoomData data;
}

[Serializable]
public class RoomData
{
    [HideInInspector]
    public List<BlockTypeData> blockTypes;
    [HideInInspector]
    public List<BlockData> blocks;
    public List<NPC> npcs = new List<NPC>();
    public Palette palette;
}

[Serializable]
public class NPC
{
    public IntVector3 position;
    public List<OrientedTileEntry> frames = new List<OrientedTileEntry>();
    public List<string> dialogues = new List<string>();

    public OrientedTileEntry GetFrame(int frame)
    {
        return frames[frame % frames.Count];
    }
}

[Serializable]
public struct BlockData
{
    public string shape;
    public IntVector3 position;
    public IntVector3 rotation;
}

public class BlockType
{
    public bool solid = true;
    public BlockShape shape;

    public Dictionary<string, List<OrientedTileEntry>> textureMapping
        = new Dictionary<string, List<OrientedTileEntry>>();
    public TextureByte texture;

    private Mesh mesh;
    private List<Vector2> texturing;
    private List<Vector2> texturing2;

    private int frame;

    public BlockType(BlockShape shape)
    {
        this.shape = shape;

        texturing = Enumerable.Repeat(Vector2.zero, shape.vertices.Count).ToList();
        texturing2 = Enumerable.Repeat(Vector2.zero, shape.vertices.Count).ToList();
    }

    public void TextureShape(string id,
                             TextureByte.Sprite sprite, 
                             SquareOrientation orientation)
    {
        texture = sprite.mTexture;

        ApplyTexturing(id, sprite, orientation);

        textureMapping[id] = new List<OrientedTileEntry> { new OrientedTileEntry
        {
            sprite = sprite,
            orientation = orientation,
        } };
    }

    public OrientedTileEntry GetFrame(string id, int frame)
    {
        var frames = textureMapping[id];

        return frames[frame % frames.Count];
    }

    private void ApplyTexturing(string id, TextureByte.Sprite sprite, SquareOrientation orientation)
    {
        var quad = shape.shapes[id];
        var uvs = sprite.UVs;

        for (int i = 0; i < quad.Count; ++i)
        {
            Vector2 coord = orientation.Transform(shape.texcoords[quad[i]]);
            Vector2 coords = Vector2.Scale(coord, uvs.size) + uvs.min;

            texturing[quad[i]] = coords;
        }
    }

    private void ApplyTexturing2(string id, TextureByte.Sprite sprite)
    {
        var quad = shape.shapes[id];
        var uvs = sprite.UVs;

        for (int i = 0; i < quad.Count; ++i)
        {
            Vector2 coord = shape.texcoords[quad[i]];
            Vector2 coords = Vector2.Scale(coord, uvs.size) + uvs.min;

            texturing2[quad[i]] = coords;
        }
    }

    public void SetFrame(int frame)
    {
        this.frame = frame;

        if (mesh == null) return;

        foreach (var pair in textureMapping)
        {
            var texturing = pair.Value[frame % pair.Value.Count];

            ApplyTexturing(pair.Key, texturing.sprite, texturing.orientation);
           ApplyTexturing2(pair.Key, Test.blankSprite);
        }

        RefreshMesh();
    }

    public Mesh CreateMesh()
    {
        if (mesh != null)
            return mesh;

        mesh = shape.CreateMesh();

        RefreshMesh();

        return mesh;
    }

    private void RefreshMesh()
    {
        mesh.SetUVs(0, texturing);
        mesh.SetUVs(1, texturing2); // TODO: extra layer?
    }
}

[Serializable]
public class OrientedTileEntry
{
    public SquareOrientation orientation;
        
    public int tile;
    [NonSerialized]
    public TextureByte.Sprite sprite;
}

[Serializable]
public class BlockTypeData
{
    [Serializable]
    public class TextureEntry
    {
        public string surface;
        public List<OrientedTileEntry> frames = new List<OrientedTileEntry>();
    }

    public string name;
    public string shape;
    public bool solid = true;
    public List<TextureEntry> texturing;
}

[Serializable]
public partial class ShapeData
{
    [Serializable]
    public struct Vertex
    {
        public Vector3 position;
        public Vector2 texture;
        
        public Vertex(Vector3 position, Vector2 texture)
        {
            this.position = position;
            this.texture = texture;
        }
    }

    [Serializable]
    public class Surface
    {
        public string name;
        public List<Vertex> triangles;
    }

    public string name;
    public List<Surface> surfaces;
}

public partial class ShapeData
{
    private static Vertex TakeShapeVertex(BlockShape shape, int index)
    {
        return new Vertex(shape.vertices[index], shape.texcoords[index]);
    }

    public static ShapeData FromBlockShape(BlockShape shape)
    {
        var data = new ShapeData
        {
            name = "no name",
            surfaces = new List<Surface>(),
        };

        foreach (var pair in shape.shapes)
        {
            var surface = new Surface
            {
                name = pair.Key,
                triangles = new List<Vertex>(),
            };

            var indicies = pair.Value;

            for (int i = 0; i < indicies.Count; i += 3)
            {
                surface.triangles.Add(TakeShapeVertex(shape, indicies[i + 0]));
                surface.triangles.Add(TakeShapeVertex(shape, indicies[i + 1]));
                surface.triangles.Add(TakeShapeVertex(shape, indicies[i + 2]));
            }

            data.surfaces.Add(surface);
        }

        return data;
    }

    private static void InsertVertex(BlockShape shape,
                                     Vertex vertex,
                                     Vector3 normal)
    {
        shape.triangles.Add(shape.triangles.Count);
        shape.vertices.Add(vertex.position);
        shape.texcoords.Add(vertex.texture);
        shape.normals.Add(normal);
    }

    public static BlockShape ToBlockShape(ShapeData data)
    {
        var shape = new BlockShape();

        foreach (var surface in data.surfaces)
        {
            int nextIndex = shape.triangles.Count;
            int vertexCount = surface.triangles.Count;

            shape.shapes.Add(surface.name, Enumerable.Range(nextIndex, vertexCount).ToList());

            for (int i = 0; i < vertexCount; i += 3)
            {
                Vertex a = surface.triangles[i + 0];
                Vertex b = surface.triangles[i + 1];
                Vertex c = surface.triangles[i + 2];

                var normal = Vector3.Cross(b.position - a.position, 
                                           c.position - a.position);

                InsertVertex(shape, a, normal);
                InsertVertex(shape, b, normal);
                InsertVertex(shape, c, normal);

                shape.tri2shape.Add(surface.name);
            }
        }

        return shape;
    }
}

public class BlockShape
{
    public List<int> triangles = new List<int>();
    public List<Vector3> vertices = new List<Vector3>();
    public List<Vector2> texcoords = new List<Vector2>();
    public List<Vector3> normals = new List<Vector3>();
    public List<string> tri2shape = new List<string>();

    public Dictionary<string, List<int>> shapes
        = new Dictionary<string, List<int>>();

    public string GetShapeFromTriangle(int triangle)
    {
        return tri2shape[triangle];
    }

    public void AddShape(string id,
                         IEnumerable<Vector3> vertices,
                         IEnumerable<Vector2> texcoords,
                         IEnumerable<int> triangles)
    {
        int offset = this.triangles.Count;

        this.vertices.AddRange(vertices);
        this.texcoords.AddRange(texcoords);
        this.triangles.AddRange(triangles.Select(i => i + offset));
    }

    public void AddQuad(string id,
                        Vector3 va, Vector2 ta,
                        Vector3 vb, Vector2 tb,
                        Vector3 vc, Vector2 tc,
                        Vector3 vd, Vector2 td)
    {
        int count = vertices.Count;

        shapes.Add(id, new List<int> { count, count + 1, count + 2,
                                       count, count + 2, count + 3 });

        Vector3 normal = Vector3.Cross(vb - va, vc - va);

        vertices.Add(va);
        vertices.Add(vb);
        vertices.Add(vc);
        vertices.Add(vd);

        normals.Add(normal);
        normals.Add(normal);
        normals.Add(normal);
        normals.Add(normal);

        texcoords.Add(ta);
        texcoords.Add(tb);
        texcoords.Add(tc);
        texcoords.Add(td);

        triangles.Add(count + 0);
        triangles.Add(count + 1);
        triangles.Add(count + 2);

        triangles.Add(count + 0);
        triangles.Add(count + 2);
        triangles.Add(count + 3);

        tri2shape.Add(id);
        tri2shape.Add(id);
    }

    public void AddTriangle(string id,
                            Vector3 va, Vector2 ta,
                            Vector3 vb, Vector2 tb,
                            Vector3 vc, Vector2 tc)
    {
        int count = vertices.Count;

        shapes.Add(id, new List<int> { count, count + 1, count + 2 });

        Vector3 normal = Vector3.Cross(vb - va, vc - va);

        vertices.Add(va);
        vertices.Add(vb);
        vertices.Add(vc);

        normals.Add(normal);
        normals.Add(normal);
        normals.Add(normal);

        texcoords.Add(ta);
        texcoords.Add(tb);
        texcoords.Add(tc);

        triangles.Add(count + 0);
        triangles.Add(count + 1);
        triangles.Add(count + 2);

        tri2shape.Add(id);
    }

    public Mesh CreateMesh()
    {
        var mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetNormals(normals);
        mesh.SetTriangles(triangles, 0);

        return mesh;
    }
}


