﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class NPCView : InstanceView<NPC>
{
    [SerializeField]
    private Test test;
    [SerializeField]
    private Text textbox;
    [SerializeField]
    private GameObject bubble;
    [SerializeField]
    private SpriteRenderer renderer;

    private int message;
    private int frame;

    public override void Refresh()
    {
        if (config.frames.Count == 0) return;

        var entry = config.frames[frame % config.frames.Count];

        transform.localPosition = config.position;
        renderer.sprite = entry.sprite.uSprite;
        renderer.flipX = entry.orientation == SquareOrientation.Flip0 || entry.orientation == SquareOrientation.None2;
        renderer.flipY = entry.orientation == SquareOrientation.Flip2 || entry.orientation == SquareOrientation.None2;
    }

    public void SetFrame(int frame)
    {
        this.frame = frame;

        Refresh();
    }

    public IEnumerator ShowMessage()
    {
        if (message == config.dialogues.Count)
        {
            message = 0;
            bubble.SetActive(false);

            yield break;
        }

        textbox.text = config.dialogues[message];
        bubble.SetActive(true);

        message = message + 1;

        yield return new WaitForSeconds(2);

        bubble.SetActive(false);
    }
}
