﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class PaletteEditWindow : MonoBehaviour 
{
    [SerializeField]
    private Image spectrumImage;
    [SerializeField]
    private Slider valueSlider;
    [SerializeField]
    private Slider2D hueSaturationSlider;

    private Palette palette;
    private int index;
    private Action<Color32> OnChanged;

    private void Awake()
    {
        valueSlider.onValueChanged.AddListener(value => UpdateColourFromUI());
        hueSaturationSlider.onUserChangedValue.AddListener(value => UpdateColourFromUI());

        var spectrum = Texture2DExtensions.Blank(16, 16);
        spectrum.filterMode = FilterMode.Bilinear;
        spectrum.MapF((x, y) => Color.HSVToRGB(x, y, 1));
        spectrum.Apply();

        spectrumImage.sprite = spectrum.FullSprite();
    }

    public void SetColor(Palette palette, int index)
    {
        this.palette = palette;
        this.index = index;

        float h, s, v;

        Color.RGBToHSV(palette.GetColor(index), out h, out s, out v);

        ignoreUI = true;
        hueSaturationSlider.value = new Vector2(h, s);
        valueSlider.value = v;
        ignoreUI = false;
    }

    private Color original;
    private bool inside;
    private bool ignoreUI;
    private void UpdateColourFromUI()
    {
        spectrumImage.color = new Color(valueSlider.value, valueSlider.value, valueSlider.value);

        if (ignoreUI) return;

        if (!inside)
        {
            inside = true;
            original = palette.GetColor(index);
        }

        palette.SetColor(index, Color.HSVToRGB(hueSaturationSlider.value.x,
                                               hueSaturationSlider.value.y,
                                               valueSlider.value));
    }
}
