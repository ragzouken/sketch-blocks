﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class TilePickerWindow : MonoBehaviour 
{
    [SerializeField]
    private InstancePoolSetup tileTogglesSetup;
    private InstancePool<TextureByte.Sprite, TileToggle> tileToggles;

    private Action<TextureByte.Sprite> OnSpritePicked;

    private void Awake()
    {
        tileTogglesSetup.Finalise(ref tileToggles);
    }

    public void Open(Action<TextureByte.Sprite> OnSpritePicked,
                     List<TextureByte.Sprite> tiles)
    {
        gameObject.SetActive(true);

        this.OnSpritePicked = OnSpritePicked;

        tileToggles.SetActive(tiles);
        tileToggles.MapActive(toggle => toggle.selected = false);
    }

    public void SelectTile(TextureByte.Sprite sprite)
    {
        OnSpritePicked?.Invoke(sprite);
    }
}
