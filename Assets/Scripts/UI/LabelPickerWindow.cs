﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class LabelPickerWindow : MonoBehaviour 
{
    [SerializeField]
    private InstancePoolSetup labelsSetup;
    private InstancePool<LabelledToggleData, LabelledToggle> labels;

    public LabelledToggleData selected { get; private set; }

    private Action<LabelledToggleData> OnSelected;
    private Action<LabelledToggleData> OnConfirmed;

    private void Awake()
    {
        labelsSetup.Finalise(ref labels);
    }

    public void Open(List<LabelledToggleData> labels,
                     Action<LabelledToggleData> OnSelected = null,
                     Action<LabelledToggleData> OnConfirmed = null,
                     LabelledToggleData selected = null)
    {
        gameObject.SetActive(true);

        this.labels.SetActive(labels);
        Select(selected ?? labels.First());
        this.OnSelected = OnSelected;
        this.OnConfirmed = OnConfirmed;
    }

    public void Select(LabelledToggleData label)
    {
        if (selected != label)
        {
            selected = label;
            OnSelected?.Invoke(label);
        }

        labels.DoIfActive(selected, v => v.Select());
    }
}
