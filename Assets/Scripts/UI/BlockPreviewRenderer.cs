﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class BlockPreviewRenderer : MonoBehaviour 
{
    public int size;

    [SerializeField]
    private BlockPreview rendererTemplate;
    private IndexedPool<BlockPreview> renderers;

    private void Awake()
    {
        renderers = new IndexedPool<BlockPreview>(rendererTemplate);
    }

    public void Clear()
    {
        renderers.SetActive(0);
    }

    public void SetBlock(int i, BlockType block)
    {
        renderers.SetActive(Mathf.Max(i + 1, renderers.count));
        renderers[i].SetIndex(i);
        renderers[i].SetBlock(block);
    }

    public void SetBlocks(List<BlockType> blocks)
    {
        renderers.SetActive(blocks.Count);
        renderers.MapActive((i, preview) =>
        {
            preview.SetBlock(blocks[i]);
            preview.SetIndex(i);
        });
    }
}
