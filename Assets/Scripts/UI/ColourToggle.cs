﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class ColourToggle : InstanceView<byte> 
{
    [SerializeField]
    private TileEditorWindow tileEditor;
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Sprite eraserSprite;

    public override void Setup()
    {
        toggle.onValueChanged.AddListener(on =>
        {
            if (on) tileEditor.SelectColour(config);
        });
    }

    public override void Refresh()
    {
        if (eraser) return;
        image.color = tileEditor.palette.GetColor(config);
    }

    private bool eraser;
    public void SetEraser()
    {
        eraser = true;
        image.sprite = eraserSprite;
        image.color = Color.white;
    }
}
