﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class EditorHUD : MonoBehaviour 
{
    [SerializeField]
    private Test test;

    [Header("Play / Edit")]
    [SerializeField]
    private GameObject playModeContainer;
    [SerializeField]
    private GameObject editModeContainer;

    [Header("Blocks / Surface")]
    [SerializeField]
    private Button blockButton;
    [SerializeField]
    private Button surfaceButton;

    [Header("Block")]
    [SerializeField]
    private GameObject blockContainer;

    [Header("Surface")]
    [SerializeField]
    private GameObject surfaceContainer;

    [Header("Character")]
    [SerializeField]
    private CanvasGroup characterGroup;

    [Header("Hotkeys")]
    [SerializeField]
    private GameObject hotkeyContainer;
    [SerializeField]
    private Text hotkeyText;

    private void Awake()
    {
        blockButton.onClick.AddListener(() => test.surfaceMode = true);
        surfaceButton.onClick.AddListener(() => test.surfaceMode = false);
    }

    private void Update()
    {
        playModeContainer.SetActive(test.Playing);
        editModeContainer.SetActive(!test.Playing);

        blockButton.gameObject.SetActive(!test.surfaceMode);
        surfaceButton.gameObject.SetActive(test.surfaceMode);

        surfaceContainer.SetActive(test.SurfaceIsSelected);
        blockContainer.SetActive(!test.surfaceMode);

        characterGroup.interactable = !test.PickingNPC && !test.PlacingNPC;
    }

    private string hotkeyTextContent;

    private void LateUpdate()
    {
        hotkeyContainer.SetActive(hotkeyTextContent != null && !test.Playing);
    }

    public void SetHotkeys(string text = null)
    {
        if (hotkeyTextContent != text && text != null)
        {
            hotkeyTextContent = text;
            hotkeyText.text = hotkeyTextContent;
        }
    }
}
