﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class TileToggle : InstanceView<TextureByte.Sprite> 
{
    [SerializeField]
    private TilePickerWindow tilePicker;
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Image image;

    public bool selected
    {
        get
        {
            return toggle.isOn;
        }

        set
        {
            toggle.isOn = value;
        }
    }

    public override void Setup()
    {
        toggle.onValueChanged.AddListener(on =>
        {
            if (on) tilePicker.SelectTile(config);
        });
    }

    public override void Refresh()
    {
        image.sprite = config.uSprite;
    }
}
