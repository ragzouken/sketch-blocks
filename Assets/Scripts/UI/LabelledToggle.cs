﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public partial class LabelledToggleData
{
    public static List<LabelledToggleData> FromStrings(params string[] strings)
    {
        return strings.Select(s => new LabelledToggleData { label = s }).ToList();
    }
}

public partial class LabelledToggleData
{
    public string label;
}

public class LabelledToggle : InstanceView<LabelledToggleData>
{
    [SerializeField]
    private LabelPickerWindow window;

    [SerializeField]
    private Text label;
    [SerializeField]
    private Toggle toggle;

    protected override void Configure()
    {
        toggle.onValueChanged.AddListener(on =>
        {
            if (on) window.Select(config);
        });
    }

    public override void Refresh()
    {
        label.text = config.label;
    }

    public void Select()
    {
        if (!toggle.isOn)
        {
            toggle.isOn = true;
        }
    }
}
