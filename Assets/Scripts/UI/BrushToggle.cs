﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class BrushToggle : InstanceView<TextureByte.Sprite> 
{
    [SerializeField]
    private TileEditorWindow tileEditor;
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Image image;

    public override void Setup()
    {
        toggle.onValueChanged.AddListener(on =>
        {
            if (on) tileEditor.SelectBrush(config);
        });
    }

    public override void Refresh()
    {
        image.sprite = config.uSprite;
    }
}
