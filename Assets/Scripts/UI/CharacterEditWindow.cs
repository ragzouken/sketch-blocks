﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;
//using InputField = TMPro.TMP_InputField;

public class CharacterEditWindow : MonoBehaviour 
{
    [SerializeField]
    private InputField dialogueInput;

    private NPC npc;

    private void Awake()
    {
        dialogueInput.onValueChanged.AddListener(Save);
    }

    public void Open(NPC npc)
    {
        this.npc = null;
        dialogueInput.text = string.Join("\n\n", npc.dialogues);
        this.npc = npc;

        gameObject.SetActive(true);
    }

    private void Save(string text)
    {
        if (npc == null) return;

        npc.dialogues = dialogueInput.text.Split(new string[] { "\n\n" }, StringSplitOptions.None).ToList();
    }
}
