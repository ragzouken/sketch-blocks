﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;
using System.Runtime.InteropServices;

public class GistSaveWindow : MonoBehaviour 
{
    [SerializeField]
    private Test test;

    [SerializeField]
    private InputField gistIDField;
    [SerializeField]
    private Button saveButton;
    [SerializeField]
    private Button loadButton;
    [SerializeField]
    private Slider progressSlider;
    [SerializeField]
    private CanvasGroup group;

    private void Awake()
    {
        saveButton.onClick.AddListener(Save);
        loadButton.onClick.AddListener(Load);
    }

    public void Open()
    {
        gameObject.SetActive(true);
        group.interactable = true;
        progressSlider.value = 0;
        gistIDField.text = "";

#if WEBGL && !UNITY_EDITOR
        gistIDField.interactable = false;
#endif
    }

    public void Close()
    {
        gameObject.SetActive(false);
        group.interactable = false;
        progressSlider.value = 0;
        gistIDField.text = "";
    }

    public void Save()
    {
        Open();

        group.interactable = false;

        test.GistRoom(id =>
        {
            group.interactable = true;
            gistIDField.text = id;

#if !UNITY_EDITOR && UNITY_WEBGL
            OpenWebSave(id);
#endif
        }, progress => progressSlider.value = progress);
    }

    public void Load()
    {
        Open();

        group.interactable = false;

#if UNITY_EDITOR || !UNITY_WEBGL
        test.Ungist(gistIDField.text, () =>
        {
            Close();
        }, progress => progressSlider.value = progress);
#else
        OpenWebLoad();
#endif
    }

#if !UNITY_EDITOR && UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void ShowLoadOverlay();
 
    [DllImport("__Internal")]
    private static extern void HideLoadOverlay();

    [DllImport("__Internal")]
    private static extern void ShowSaveOverlay(string id);

    [DllImport("__Internal")]
    private static extern void HideSaveOverlay();

    public void OpenWebLoad()
    {
        ToggleCaptureAllInput(false);
        ShowLoadOverlay();
    }

    public void OpenWebSave(string id)
    {
        ToggleCaptureAllInput(false);
        ShowSaveOverlay(id);
    }

    public void OnWebClickedLoad(string id)
    {
        ToggleCaptureAllInput(true);
        HideLoadOverlay();

        test.Ungist(id, () => Close());
    }

    public void OnWebClickedSave(string _)
    {
        HideSaveOverlay();
        HideLoadOverlay();

        Save();
    }

    public void OnWebClickedCloseLoad(string _)
    {
        HideLoadOverlay();
        ToggleCaptureAllInput(true);
        Close();
    }

    public void OnWebClickedCloseSave(string _)
    {
        HideSaveOverlay();
        ToggleCaptureAllInput(true);

        Close();
    }

    private void ToggleCaptureAllInput(bool captureAll)
    {
        WebGLInput.captureAllKeyboardInput = captureAll;
    }
#endif
}
