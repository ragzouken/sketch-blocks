﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class BlockPickerWindow : MonoBehaviour 
{
    [SerializeField]
    private BlockPreviewRenderer previewRenderer;

    [SerializeField]
    private InstancePoolSetup blockThumbnailsSetup;
    private InstancePool<int, BlockThumbnail> blockThumbnails;

    private Action<BlockType> OnBlockPicked;

    private List<BlockType> _blocks;

    private void Awake()
    {
        blockThumbnailsSetup.Finalise(ref blockThumbnails);
    }

    public void Open(Action<BlockType> OnBlockPicked,
                     List<BlockType> blocks)
    {
        _blocks = blocks;

        gameObject.SetActive(true);

        this.OnBlockPicked = OnBlockPicked;

        SetBlocks(blocks);
    }

    public void Close()
    {
        gameObject.SetActive(false);
        previewRenderer.Clear();
    }

    public void SelectBlock(int block)
    {
        OnBlockPicked(_blocks[block]);
    }

    public void PreviewBlock(BlockType block)
    {
        previewRenderer.SetBlock(0, block);
    }

    public void SetBlocks(List<BlockType> blocks)
    {
        previewRenderer.SetBlocks(blocks);

        blockThumbnails.SetActive(Enumerable.Range(0, blocks.Count).ToList());
        blockThumbnails.MapActive(toggle => toggle.selected = false);
    }
}
