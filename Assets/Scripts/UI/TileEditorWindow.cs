﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using static UnityEngine.RectTransformUtility;

using Random = UnityEngine.Random;

public class TileEditorWindow : MonoBehaviour 
{
    [SerializeField]
    private InstancePoolSetup colourTogglesSetup;
    private InstancePool<byte, ColourToggle> colourToggles;

    [SerializeField]
    private InstancePoolSetup brushTogglesSetup;
    private InstancePool<TextureByte.Sprite> brushToggles;

    [SerializeField]
    private Image canvasImage;
    [SerializeField]
    private Image cursorImage;

    [SerializeField]
    private PaletteEditWindow paletteWindow;
    [SerializeField]
    private Toggle paletteEditToggle;

    private List<TextureByte.Sprite> brushes 
        = new List<TextureByte.Sprite>();
    private TextureByte.Sprite preview;

    public Palette palette { get; private set; }
    public TextureByte.Sprite canvas { get; private set; }
    public TextureByte.Sprite brush { get; private set; }
    public byte colour { get; private set; }

    private bool dragging;
    private Vector2 prev, next;

    private void Awake()
    {
        colourTogglesSetup.Finalise(ref colourToggles);
        brushTogglesSetup.Finalise(ref brushToggles);
    }

    private void Start()
    {
        int count = 8;

        var texture = new TextureByte(16 * (count + 1), 16);

        for (int i = 0; i < count; ++i)
        {
            var brush = texture.GetSprite(new IntRect(i * 16, 0, 16, 16), IntVector2.one * 8);

            TextureByte.Draw.Circle(brush, IntVector2.zero, 1, i + 1, TextureByte.mask);

            brushes.Add(brush);
        }

        preview = texture.GetSprite(new IntRect(count * 16, 0, 16, 16), IntVector2.one * 8);

        texture.Apply();

        var colors = Enumerable.Range(0, 16).Select(i => (byte) i).ToList();

        brushToggles.SetActive(brushes);
        colourToggles.SetActive(colors);
        colourToggles.Get(0).SetEraser();

        SelectBrush(brushes[0]);
    }

    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            dragging = false;
        }

        Vector2 mouse = Input.mousePosition;
        Vector2 local;

        if (ScreenPointToLocalPointInRectangle(canvasImage.rectTransform,
                                               mouse,
                                               null,
                                               out local))
        {
            byte flash = (byte) (1 + (Time.timeSinceLevelLoad * 16) % 255);

            var blend = TextureByte.MakeRecolorBlend(colour);
            var pblend = TextureByte.MakeRecolorBlend(colour == 0 ? (byte) flash : colour);


            Vector2 clamp = local;
            clamp.x = Mathf.Floor(clamp.x / 8f) * 8;
            clamp.y = Mathf.Floor(clamp.y / 8f) * 8;

            preview.Clear();
            preview.Blend(brush, pblend);
            preview.Apply();

            cursorImage.sprite = preview.uSprite;
            cursorImage.rectTransform.anchoredPosition = clamp;

            local.x /= canvasImage.rectTransform.rect.width;
            local.y /= canvasImage.rectTransform.rect.height;

            local.x *= canvas.rect.width;
            local.y *= canvas.rect.height;

            next = local;

            if (dragging)
            {
                var line = TextureByte.Draw.Sweep(brush, prev, next, TextureByte.mask);
                canvas.Blend(line, blend, canvasPosition: IntVector2.one * 16);
                canvas.Apply();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                dragging = true;
            }

            prev = next;
        }

        colourToggles.Refresh();
    }

    public void SelectBrush(TextureByte.Sprite brush)
    {
        this.brush = brush;
    }

    public void SelectColour(byte colour)
    {
        this.colour = colour;

        for (int i = 0; i < brushes.Count; ++i)
        {
            TextureByte.Draw.Circle(brushes[i], IntVector2.zero, colour, i + 1, TextureByte.mask);
        }

        brushes[0].Apply();

        paletteWindow.SetColor(palette, Mathf.Max(1, colour));

        if (colour == 0) paletteEditToggle.isOn = false;
    }

    public void SetCanvas(TextureByte.Sprite canvas, Palette palette)
    {
        this.canvas = canvas;
        this.palette = palette;

        canvasImage.sprite = canvas.uSprite;
    }
}
