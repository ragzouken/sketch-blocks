﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class BlockThumbnail : InstanceView<int>
{
    [SerializeField]
    private BlockPickerWindow picker;
    [SerializeField]
    private BlockPreviewRenderer previewRenderer;

    [SerializeField]
    private RawImage image;
    [SerializeField]
    private Toggle toggle;

    public bool selected
    {
        get
        {
            return toggle.isOn;
        }

        set
        {
            toggle.isOn = value;
        }
    }

    public override void Setup()
    {
        toggle.onValueChanged.AddListener(on =>
        {
            if (on) picker.SelectBlock(config);
        });
    }

    public override void Refresh()
    {
        int x = config % previewRenderer.size;
        int y = config / previewRenderer.size;

        float s = 1f / previewRenderer.size;

        image.uvRect = new Rect(x * s, y * s, s, s);
    }
}
