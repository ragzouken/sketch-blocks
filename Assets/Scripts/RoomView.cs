﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class RoomView : InstanceView<Room>
{
    [SerializeField]
    private InstancePoolSetup blocksSetup;
    private InstancePool<Block, BlockView> blocks;

    [SerializeField]
    private InstancePoolSetup blockCubesSetup;
    private InstancePool<Block> blockCubes;

    [SerializeField]
    private InstancePoolSetup npcsSetup;
    public InstancePool<NPC, NPCView> npcs;

    private void Awake()
    {
        blocksSetup.Finalise(ref blocks, sort: false);
        blockCubesSetup.Finalise(ref blockCubes, sort: false);
        npcsSetup.Finalise(ref npcs, sort: false);
    }

    public override void Refresh()
    {
        blocks.SetActive(config.blocks);
        blockCubes.SetActive(config.blocks);
        npcs.SetActive(config.data.npcs);

        blocks.Refresh();
        blockCubes.Refresh();
        npcs.Refresh();
    }
}
