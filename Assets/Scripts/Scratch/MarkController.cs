﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

[RequireComponent(typeof(CharacterController))]
public class MarkController : MonoBehaviour 
{
    [Header("Vertical")]
    [SerializeField]
    private float gravity;
    [SerializeField]
    private float jumpPower;
    [SerializeField]
    private float jumpForgive;

    private CharacterController controller;
    private ControllerColliderHit colliderHit;

    private Vector3 velocity;

    private bool onFloor;
    private float timeSinceFloor;

    #region Raycasts

    private RaycastHit[] hits = new RaycastHit[1024];

    private void FilterHits(ref int count, Func<RaycastHit, bool> keep)
    {
        int i = 0;

        while (i < count)
        {
            if (!keep(hits[i]))
            {
                hits[i] = hits[count - 1];
                count -= 1;
            }
            else
            {
                i += 1;
            }
        }
    }

    private RaycastHit ClosestHit(int count)
    {
        RaycastHit best = hits[0];

        for (int i = 1; i < count; ++i)
        {
            if (hits[i].distance < best.distance)
            {
                best = hits[i];
            }
        }

        return best;
    }

    private bool GetFloor(Vector3 position, out RaycastHit floor, float offset = 0)
    {
        floor = default(RaycastHit);

        var ray = new Ray(position + Vector3.up * offset, Vector3.down);
        int count = Physics.RaycastNonAlloc(ray, hits);

        FilterHits(ref count, hit => hit.normal.y > 0);

        for (int i = 0; i < count; ++i)
        {
            hits[i].distance -= offset;
        }

        if (count == 0) return false;

        floor = ClosestHit(count);
        return true;
    }

    #endregion

    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    private void OnControllerColliderHit (ControllerColliderHit hit)
    {
        colliderHit = hit;
    }

    private void TouchFloor()
    {
        onFloor = true;
        timeSinceFloor = 0;
    }

    private void LeaveFloor()
    {
        onFloor = false;
    }

    private void FixedUpdate()
    {
        float speed = 2f;

        Vector2 input = Vector2.zero;

        if (Input.GetKey(KeyCode.W))
        {
            input.y = speed; 
        }
        else if (Input.GetKey(KeyCode.S))
        {
            input.y = -speed; 
        }

        if (Input.GetKey(KeyCode.A))
        {
            input.x = -speed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            input.x = speed;
        }

        if (Input.GetKeyDown(KeyCode.Space) && timeSinceFloor < jumpForgive)
        {
            LeaveFloor();
            velocity.y = jumpPower;
        }

        input = Quaternion.Euler(0, 0, -transform.eulerAngles.y) * input;

        velocity = new Vector3(input.x, velocity.y, input.y);
        velocity.y -= gravity * Time.fixedDeltaTime;

        var flags = controller.Move(velocity * Time.fixedDeltaTime);

        bool floor = (flags & CollisionFlags.Below) != 0;

        if (floor)
        {
            TouchFloor();
        }
        else
        {
            LeaveFloor();
        }

        if (!onFloor)
        {
            timeSinceFloor += Time.fixedDeltaTime;
        }
        else
        {
            timeSinceFloor = 0;
        }
    }
}
