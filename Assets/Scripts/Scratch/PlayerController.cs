﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour 
{
    private CharacterController controller;

    public Transform cameraPivot;

    private RaycastHit[] hits = new RaycastHit[1024];

    private Vector2 velocity;

    private float yVel = 0;
    private float ySpeed;

    private float fallMax = 10f;
    [SerializeField]
    private float gravity = 10f;

    public float jumpForgive   = .5f;
    public float stepUpLimit   = .55f;
    public float stepDownLimit = .55f;

    private int steps = 1;

    [Header("Live Values")]
    [SerializeField]
    private bool onFloor;
    [SerializeField]
    private float timeSinceFloor;

    private float height => controller.height;
    private float offset => height * 0.5f;

    private static int floorMask;

    private void Start()
    {
        controller = GetComponent<CharacterController>();

        floorMask = LayerMask.GetMask("Floor");

        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), 
                                     LayerMask.NameToLayer("Cubes"));
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), 
                                     LayerMask.NameToLayer("Passable"));
    }

    private void FilterHits(ref int count, Func<RaycastHit, bool> keep)
    {
        int i = 0;

        while (i < count)
        {
            if (!keep(hits[i]))
            {
                hits[i] = hits[count - 1];
                count -= 1;
            }
            else
            {
                i += 1;
            }
        }
    }

    private RaycastHit ClosestHit(int count)
    {
        RaycastHit best = hits[0];

        for (int i = 1; i < count; ++i)
        {
            if (hits[i].distance < best.distance)
            {
                best = hits[i];
            }
        }

        return best;
    }

    private bool GetFloor(Vector3 position, out RaycastHit floor, float offset = 0)
    {
        floor = default(RaycastHit);

        var ray = new Ray(position + Vector3.up * offset, Vector3.down);
        int count = Physics.RaycastNonAlloc(ray, hits, Mathf.Infinity, floorMask);

        FilterHits(ref count, hit => hit.normal.y > 0);

        for (int i = 0; i < count; ++i)
        {
            hits[i].distance -= offset;
        }

        if (count == 0) return false;

        floor = ClosestHit(count);
        return true;
    }

    private void FallToFloor()
    {
        RaycastHit floor;

        if (GetFloor(transform.position, out floor, offset))
        {
            float delta = floor.distance - offset;

            if (delta > 0 && delta <= Time.fixedDeltaTime * Mathf.Abs(yVel))
            {
                Debug.Log("fall 2 floor");
                
                controller.Move(Vector3.down * delta);

                CheckFloor();
                //TouchFloor();
            }
        }
    }

    private void StepUpToFloor()
    {
        RaycastHit floor;

        if (GetFloor(transform.position, out floor, offset: stepUpLimit)
         && Mathf.Abs(floor.distance) > 0.01f
         && floor.distance < stepUpLimit)
        {
            //controller.Move(Vector3.down * (floor.distance - offset));

            TouchFloor();

            Debug.LogFormat("step up {0}", floor.distance);
        }
    }

    private void StepDownToFloor()
    {
        RaycastHit floor;

        if (GetFloor(transform.position, out floor)
         && Mathf.Abs(floor.distance) > 0.01f
         && floor.distance < stepDownLimit)
        {
            //controller.Move(Vector3.down * (floor.distance - offset));

            TouchFloor();

            Debug.LogFormat("step down {0}", floor.distance);
        }
    }

    private void TouchFloor()
    {
        onFloor = true;
        yVel = 0;
        timeSinceFloor = 0;
    }

    private void CheckFloor()
    {
        RaycastHit floor;

        onFloor = GetFloor(transform.position, out floor, offset) 
               && floor.distance <= offset + controller.skinWidth * 2;

        onFloor |= controller.isGrounded;

        if (onFloor)
        {
            TouchFloor();
        }
    }

    private bool canJump
    {
        get
        {
            return onFloor || timeSinceFloor <= jumpForgive;
        }
    }

    private void FixedUpdate()
    {
        float speed = 2f;

        bool move = !Input.GetKey(KeyCode.LeftShift) && !Test.IsPointBlocked(Input.mousePosition);

        if (Input.GetKey(KeyCode.W) && move)
        {
            velocity.y = speed; 
        }
        else if (Input.GetKey(KeyCode.S) && move)
        {
            velocity.y = -speed; 
        }
        else
        {
            velocity.y = 0;
        }

        if (Input.GetKey(KeyCode.A) && move)
        {
            velocity.x = -speed;
        }
        else if (Input.GetKey(KeyCode.D) && move)
        {
            velocity.x = speed;
        }
        else
        {
            velocity.x = 0;
        }

        velocity = Quaternion.Euler(0, 0, -transform.eulerAngles.y) * velocity;

        Vector2 vel = velocity / steps * Time.fixedDeltaTime;

        
        CheckFloor();

        for (int i = 0; i < steps; ++i)
        {
            controller.Move(new Vector3(vel.x, 0, vel.y));

            if (onFloor)
            {
                //StepDownToFloor();
                //StepUpToFloor();
            }            
        }

        if (!onFloor)
        {
            float slice = Time.fixedDeltaTime / steps;

            for (int i = 0; i < steps; ++i)
            {
                yVel -= gravity * slice;

                FallToFloor();

                controller.Move(Vector3.up * yVel * slice);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && canJump && move)
        {
            onFloor = false;
            yVel = 5;
            controller.Move(Vector3.up * yVel * Time.fixedDeltaTime);
            timeSinceFloor += jumpForgive;
        }

        if (!onFloor)
        {
            timeSinceFloor += Time.fixedDeltaTime;
        }
    }
}
