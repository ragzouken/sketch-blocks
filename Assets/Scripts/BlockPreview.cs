﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class BlockPreview : InstanceView<BlockType>
{
    [SerializeField]
    private BlockPreviewRenderer previews;

    [SerializeField]
    private new MeshRenderer renderer;
    [SerializeField]
    private MeshFilter filter;
    [SerializeField]
    private new Camera camera;
    [SerializeField]
    private Transform spinner;

    public BlockType block;

    private MaterialPropertyBlock materialProperties;

    private void Awake()
    {
        materialProperties = new MaterialPropertyBlock();
    }

    private void Update()
    {
        spinner.Rotate(Vector3.up, Time.deltaTime * 90);
    }

    public void SetIndex(int i)
    {
        int x = i % previews.size;
        int y = i / previews.size;

        float s = 1f / previews.size;

        transform.localPosition = new Vector3(x, y, 0) * 5;
        camera.rect = new Rect(x * s, y * s, s, s);
    }

    public void SetBlock(BlockType type)
    {
        var mesh = type.CreateMesh();

        block = type;
        
        filter.sharedMesh = mesh;

        renderer.GetPropertyBlock(materialProperties);
        materialProperties.SetTexture("_MainTex", block.texture.uTexture);
        renderer.SetPropertyBlock(materialProperties);
    }

    public override void Refresh()
    {
        SetBlock(config);
    }
}
