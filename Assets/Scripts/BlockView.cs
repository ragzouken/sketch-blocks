﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class Block
{
    public IntVector3 position;
    public int rotation1, rotation2, rotation3;
    public BlockType type;
}

public class BlockView : InstanceView<Block>
{
    [SerializeField]
    private new MeshRenderer renderer;
    [SerializeField]
    private MeshFilter filter;
    [SerializeField]
    private new MeshCollider collider;

    public BlockType block;

    private MaterialPropertyBlock materialProperties;

    private static int solidLayer;
    private static int passLayer;

    private int _layer = -1;
    private int layer
    {
        get
        {
            return _layer;
        }

        set
        {
            if (_layer != value)
            {
                _layer = value;
                collider.gameObject.layer = value;
            }
        }
    }

    private void Awake()
    {
        solidLayer = LayerMask.NameToLayer("Floor");
        passLayer = LayerMask.NameToLayer("Passable");

        materialProperties = new MaterialPropertyBlock();
    }

    public void SetBlock(BlockType type)
    {
        if (block != type)
        {
            block = type;
            ChangeBlockType();
        }

        layer = type.solid ? solidLayer : passLayer;
    }

    private void ChangeBlockType()
    {
        var mesh = block.CreateMesh();

        filter.sharedMesh = mesh;
        collider.sharedMesh = mesh;

        renderer.GetPropertyBlock(materialProperties);
        materialProperties.SetTexture("_MainTex", block.texture.uTexture);
        renderer.SetPropertyBlock(materialProperties);
    }

    public override void Refresh()
    {
        SetBlock(config.type);

        transform.localPosition = config.position;
        transform.localRotation = Quaternion.Euler(config.rotation1 * 90, config.rotation2 * 90, config.rotation3 * 90);
    }
}
