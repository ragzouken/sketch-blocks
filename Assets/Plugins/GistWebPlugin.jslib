﻿var GistWebPlugin = {
    ShowLoadOverlay: function()
    {
        var overlay = document.getElementById('gistLoadOverlay');
        var input = document.getElementById('gistLoadIDText');

        overlay.setAttribute('style','display:block;');
        input.value = "";
        input.focus();
    },
    HideLoadOverlay: function()
    {
        var overlay = document.getElementById('gistLoadOverlay');

        overlay.setAttribute('style','display:none;');
    },

    ShowSaveOverlay: function(idPointer)
    {
        var SelectText = function(text) {
            var doc = document, range, selection;

            if (doc.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                selection = window.getSelection();        
                range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        };

        var overlay = document.getElementById('gistSaveOverlay');
        var text = document.getElementById('gistSaveIDText');

        text.textContent = Pointer_stringify(idPointer);
        SelectText(text);
        overlay.setAttribute('style', 'display: block');
        overlay.focus();
    },

    HideSaveOverlay: function()
    {
        document.getElementById('gistSaveOverlay').setAttribute('style', 'display: none');
    },

    UpdateSearchWithID: function(id)
    {
        var str = Pointer_stringify(id);
        window.history.pushState("object or string", "project saved at id", "?id=" + str);
    },

    GetWindowSearch: function()
    {
        var str = window.location.search.replace("?", "");
        var buffer = _malloc(lengthBytesUTF8(str) + 1);
        writeStringToMemory(str, buffer);
        return buffer;
    } 
};
mergeInto(LibraryManager.library, GistWebPlugin);