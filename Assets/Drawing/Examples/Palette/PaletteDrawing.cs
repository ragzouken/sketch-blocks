﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public class PaletteDrawing : MonoBehaviour 
{
    public Texture2D paletteTexture;

    public SpriteRenderer drawing;
    private TextureByte.Sprite canvasSprite;

    private new Collider collider;

    private bool dragging;
    private Vector2 prevMouse, nextMouse;

    [Header("Controls")]
    [SerializeField]
    [Range(1, 8)]
    private int thickness;
    [SerializeField]
    [Range(1, 255)]
    private int index;
    [SerializeField]
    private Color color;

    private int prevIndex;
    private Palette palette;

    private void Awake()
    {
        // create a 512x512 pixel canvas to draw on
        canvasSprite = TextureByte.Draw.GetSprite(512, 512);
        // tell unity that for every 512 pixels wide this sprite is, it should
        // be one world unit wide
        canvasSprite.SetPixelsPerUnit(512);
        // fill the sprite with palette color 0
        canvasSprite.Clear(0);
        // update the real unity texture so that it can be displayed
        canvasSprite.mTexture.Apply();

        // set the existing SpriteRenderer to use the newly created sprite
        drawing.sprite = canvasSprite.uSprite;

        collider = drawing.GetComponent<Collider>();

        palette = new Palette(paletteTexture);

        // randomise palette
        for (int i = 1; i < 256; ++i)
        {
            palette.SetColor(i, new Color(Random.value, Random.value, Random.value), apply: false);
        }

        palette.Apply();
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider == collider)
            {
                nextMouse = (Vector2) hit.point * 512;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (!dragging)
            {
                dragging = true;
            }
            else
            {
                // draw a line onto the canvas sprite using alpha blending, 
                // then apply the changes to the real unity texture
                TextureByte.Draw.Line(canvasSprite, prevMouse, nextMouse, (byte) index, thickness, TextureByte.mask);
                canvasSprite.Apply();
            }
        }
        else
        {
            dragging = false;
        }

        prevMouse = nextMouse;

        if (prevIndex != index)
        {
            color = palette.GetColor(index);
            prevIndex = index;
        }
        else
        {
            palette.SetColor(index, color);
        }
    }
}
