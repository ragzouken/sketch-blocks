﻿#ifndef DRAWING_PALETTE_INCLUDED
#define DRAWING_PALETTE_INCLUDED

inline fixed4 pal2Dindex(sampler2D palette, uint index)
{
	// convert index to 2d coordinates
    uint x = index % 16;
    uint y = index / 16;
    
	float x2 = x;
	float y2 = y;

    // sample color from palette texture (with corrections)
    fixed4 col = tex2D(palette, float2((x2 + .5) / 16., (y2 + .5) / 16));

	return col;
}

inline fixed4 pal2Dtex(sampler2D palette, sampler2D tex, float2 uv)
{
    // sample texture
    fixed4 col = tex2D(tex, uv);
    // use alpha channel as index
    uint index = abs(floor(col.a * 255));
    
    // return corresponding colour from the palette
    return pal2Dindex(palette, index);
}

#endif