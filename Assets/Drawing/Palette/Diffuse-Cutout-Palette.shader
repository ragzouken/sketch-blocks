﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Drawing/CutoutDiffusePalette" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_MainTex2 ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Palette ("Palette", 2D) = "white" {}
    _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
}

SubShader {
    Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Geometry"}
    LOD 200

CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff addshadow // vertex:vert 

#include "DrawingPalette.cginc"

sampler2D _MainTex;
sampler2D _Palette;
fixed4 _Color;

float _Amount;

struct Input {
    float2 uv_MainTex;
	float2 uv2_MainTex2;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = pal2Dtex(_Palette, _MainTex, IN.uv_MainTex) * _Color;
    o.Albedo = c.rgb;
    o.Alpha = c.a;
}
ENDCG
}
}
