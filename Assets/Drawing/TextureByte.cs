﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TextureByte : ManagedTexture<TextureByte, byte>
{
    public static Blend<byte> mask = (canvas, brush) => brush == 0 ? canvas : brush;
    public static Blend<byte> replace = (canvas, brush) => brush;
    public static DrawTools Draw = new DrawTools(mask, 0);

    public static Blend<byte> MakeRecolorBlend(byte color) => (canvas, brush) => brush == 0 ? canvas : color;

    public static byte Lerp(byte a, byte b, byte u)
    {
        return (byte)(a + ((u * (b - a)) >> 8));
    }

    public static Texture2D temporary;

    static TextureByte()
    {
        temporary = Texture2DExtensions.Blank(1, 1);
    }

    public TextureByte() : base(TextureFormat.Alpha8) { }

    public TextureByte(int width, int height) 
        : base(width, height, TextureFormat.Alpha8)
    {
    }

    public override void ApplyPixels()
    {
        uTexture.LoadRawTextureData(pixels);
    }

    public void SetPixels32(Color32[] pixels)
    {
        for (int i = 0; i < this.pixels.Length; ++i)
        {
            this.pixels[i] = (byte) (pixels[i].a + pixels[i].r + pixels[i].g + pixels[i].b);
        }

        dirty = true;
    }

    public void DecodeFromPNG(byte[] data)
    {
        temporary.LoadImage(data);
        SetPixels32(temporary.GetPixels32());

        Apply();
    }

    public void DecodeFromPNG(byte[] data, Palette palette)
    {
        var map = new Dictionary<Color32, byte>();

        temporary.LoadImage(data);
        var colors = temporary.GetPixels32();

        for (int i = 0; i < colors.Length; ++i)
        {
            Color32 color = colors[i];
            byte index;

            if (!map.TryGetValue(color, out index))
            {
                index = color.a == 0 ? (byte) 0 : (byte) UnityEngine.Random.Range(1, 256);
                map[color] = index;
            }

            pixels[i] = index;
        }
        
        dirty = true;

        Apply();
    }

    public byte[] ExportToPNG(Palette palette)
    {
        var colors = new Color32[pixels.Length];

        for (int i = 0; i < colors.Length; ++i)
        {
            colors[i] = palette.GetColor(pixels[i]);
        }

        temporary.SetPixels32(colors);

        return temporary.EncodeToPNG();
    }
}
