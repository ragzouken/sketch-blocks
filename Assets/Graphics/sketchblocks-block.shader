﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "sketchblocks/block" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_MainTex2 ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Palette ("Palette", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}

	SubShader 
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Geometry"}

		CGPROGRAM
		#pragma surface surf Lambert alphatest:_Cutoff addshadow // vertex:vert 

		#include "../Drawing/Palette/DrawingPalette.cginc"

		sampler2D _MainTex;
		sampler2D _Palette;
		fixed4 _Color;

		float _Amount;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_MainTex2;
		};

		/*
		void vert(inout appdata_full v)
		{
			float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
			worldPos.y += fmod(abs(.57 + worldPos.x * .283 + worldPos.z * .171), 1) * _Amount;
			v.vertex = mul(unity_WorldToObject, worldPos);
		}
		*/

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c1 = pal2Dtex(_Palette, _MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = pal2Dtex(_Palette, _MainTex, IN.uv2_MainTex2) * _Color;

			fixed4 c = lerp(c1, c2, c2.a);

			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
}
